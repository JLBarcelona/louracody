$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function validation(form, err){
	let inputs = $('#'+form+' .form-control');
	inputs.each(function(index){
		let id = $(this).attr('id');
    	$("#"+id).removeClass('is-invalid');
    	let exist = document.getElementById('err_'+id);
    	let error = Object.assign({}, err);

    	if (Object.hasOwn(error, id)) {
    		let message = (err[id] !== 'undefined')? err[id] : '';
    		if (exist) {
				$('#err_'+id).text(message);
        	}else{
        		$("#"+id).parent().append('<div class="invalid-feedback" id="err_'+id+'">'+ message+'</div>');
        	}
    		$("#"+id).addClass('is-invalid');
    	}else{
    		$("#"+id).removeClass('is-invalid');
    	}
	});
}

$(document).ready(function(){
  $("form.needs-validation :input").on('input', function(){
      var _this = $(this);
      var checkClass = _this.hasClass('is-invalid');
      if (checkClass) {
        _this.removeClass('is-invalid');
      }
  });
});


$(".select2").each(function(e){
var select2;
let values = $(this).data('value');
let type = $(this).data('type');

if (type == 'multiple') {
  select2 = $(this).select2({
    tags: true
  });
  $(this).val(values).trigger('change');
}else{
  select2 = $(this).select2();
}

  // Check if have child or nested selection
  if (typeof $(this).data('select2-child') !== 'undefined') {
   $(this).val(values).trigger('change');
    var child =  $($(this).data('select2-child'));
      $(this).on('select2:select', function (e) {
      var data = e.params.data;
      let urlOption = child.data('select2-url')+'/'+data.id;
      console.log(urlOption);
      $.ajax({
          type:"GET",
          url:urlOption,
          data:{},
          dataType:'json',
          beforeSend:function(){
            child.prop('disabled', true);
          },
          success:function(response){
            let dataResponse = response.data;
            let HtmlOption = '';
            console.log($($(this).data('select2-placeholder')));
            if (child.data('select2-placeholder')) {
              HtmlOption += '<option value="" selected disabled>'+child.data('select2-placeholder')+'</option>';  
            }
            $.each(dataResponse, function(key, value){
              HtmlOption += '<option value="'+value.id+'">'+value.name+'</option>';
            }); 

            child.html(HtmlOption);
            child.prop('disabled', false);
          },
          error: function(error){
            console.log(error);
          }
        });
    });
  }

  if ($(this).data('select2-parent')) {
      let me = $(this);
      let parent = me.data('select2-parent');
      let param = $(parent).data('value');
      let urlOption = me.data('select2-url')+'/'+param;


    if (param == '' || param == null) {

    }else{
      $.ajax({
          type:"GET",
          url:urlOption,
          data:{},
          dataType:'json',
          beforeSend:function(){
            me.prop('disabled', true);
          },
          success:function(response){
            console.log(response);
            let dataResponse = response.data;
            let HtmlOption = '';
            if (me.data('select2-placeholder')) {
              HtmlOption += '<option value="" selected disabled>'+me.data('select2-placeholder')+'</option>'; 
            }
            $.each(dataResponse, function(key, value){
              HtmlOption += '<option value="'+value.id+'">'+value.name+'</option>';
            }); 

            me.html(HtmlOption);
            me.prop('disabled', false);
            let data_selected = me.data('value');
          me.val(data_selected).trigger('change');
          },
          error: function(error){
            console.log(error);
          }
        });
    }
  }else{
   $(this).val(values).trigger('change');
  }
});



 function swalLoad(icon, title, text, redirect = '', timer = 2000){
    Swal.fire({
      icon : icon,
      title: title,
      text: text,
      timer: timer,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
      willClose: () => {

      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        if (redirect !== '' || redirect !== null) {
          window.location = redirect;
        }
      }
    })
  }


  function password_toggler(_this, selector){
    $(_this).toggleClass('fa-eye fa-eye-slash')
    $(selector).attr('type', function(index, attr){
        return attr == 'text' ? 'password' : 'text';
    });
  }


function handleFileSelect(e, selector) {
    var files = e.target.files;
    var filesArr = Array.prototype.slice.call(files);
    filesArr.forEach(function (f) {
      if (!f.type.match("image.*")) {
        return;
      }

      var reader = new FileReader();
      reader.onload = function (e) {
        if (selector.data('preview-type') === 'background') {
           selector.attr('style', 'background-image:url(\''+e.target.result+'\')');
        }else{
           selector.attr('src', e.target.result);
        }
         selector.attr('data-file', f.name);
      };
      reader.readAsDataURL(f);
    });
  }

 $(".preview-upload").each(function(e){
    $(this).on('change', function(event){
      let selector = $("#"+$(this).data('preview-selector'));
      handleFileSelect(event, selector);
    });
  });

// to render select input value must add attribute data-value="value"
$('.form-select').each(function(){
  let val = $(this).data('value');
  $(this).val(val);
});


function copyToClipboard(element) {
    $(element+'_copy').text('Copied!');
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    setTimeout(function(){
        $(element+'_copy').text('');
    }, 400);
    // swal("Copied",'',"success")
}


function copyToClipboardLink(element) {
    let text = $(element+'_txt').text();
    $(element+'_txt').text('Copied!');
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).data('link')).select();
    document.execCommand("copy");
    $temp.remove();
    setTimeout(function(){
        $(element+'_txt').text(text);
    }, 400);
    // swal("Copied",'',"success")
}