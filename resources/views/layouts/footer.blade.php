<footer class="footer pt-lg-5 pt-4 bg-dark text-light">
      <div class="container mb-4 py-4 pb-lg-5">
        <div class="row gy-4 justify-content-center">
          <!-- Subscription form-->
          <div class="col-lg-4 text-center">
            <h3 class="h4 text-light">Please Support our Website</h3>
            <p class="fs-sm mb-4 opacity-60">Your support means a lot. Thank you!</p>
            <a class="btn btn-primary btn-sm rounded-pill" href="https://paypal.me/JLBarcelona?country.x=PH&locale.x=en_US">Donate</a>
          </div>
        </div>
      </div>
      <div class="py-4 border-top border-light">
        <div class="container d-flex flex-column flex-lg-row align-items-center justify-content-between py-2">
          <!-- Copyright-->
          <p class="order-lg-1 order-2 fs-sm mb-2 mb-lg-0"><span class="text-light opacity-60">&copy; All rights reserved. Powered By </span>Barcetech</p>
          <div class="d-flex flex-lg-row flex-column align-items-center order-lg-2 order-1 ms-lg-4 mb-lg-0 mb-4">
            <!-- Links-->
            <div class="d-flex flex-wrap fs-sm mb-lg-0 mb-4 pe-lg-4"><a class="nav-link-light px-2 mx-1" href="{{ route('privacy') }}">Privacy Policy</a>
           <a class="nav-link-light px-2 mx-1" href="{{ route('terms') }}">
Terms & Conditions</a></div> 
            <div class="d-flex align-items-center">
           <!--  
              <div class="ms-4 ps-lg-2 text-nowrap"><a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="#" target="_blank"><i class="fi-facebook"></i></a><a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="#"><i class="fi-youtube"></i></a></div> -->
            </div>
          </div>
        </div>
      </div>


    </footer>