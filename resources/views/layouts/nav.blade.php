<!-- Navbar with user account dropdown -->
<header class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
  <div class="container">
    <a href="{{ url('/') }}" class="navbar-brand flex-shrink-0 me-2 me-xl-4">
      <img class="d-block" src="{{ asset('img/logo-nav.png') }}" width="170" alt="Finder">
    </a>
    <button type="button" class="navbar-toggler ms-auto" data-bs-toggle="collapse" data-bs-target="#navbarUserNav" aria-controls="navbarUserNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="dropdown order-lg-3">
      <a href="#" class="d-inline-block py-1" data-bs-toggle="dropdown">
        <img class="rounded-circle" src="{{ asset('finder/img/avatars/38.png') }}" width="40" alt="User">
      </a>
      <div class="dropdown-menu dropdown-menu-end">
        <div class="d-flex align-items-start border-bottom px-3 py-1 mb-2" style="width: 16rem;">
          <img class="rounded-circle" src="{{ asset('finder/img/avatars/38.png') }}" width="48" alt="User">
          <div class="ps-2">
            <h6 class="fs-base mb-0">{{ Auth::user()->name }}</h6>
            <div class="star-rating star-rating-sm">
              <i class="star-rating-icon fi-star-filled active"></i>
              <i class="star-rating-icon fi-star-filled active"></i>
              <i class="star-rating-icon fi-star-filled active"></i>
              <i class="star-rating-icon fi-star-filled active"></i>
              <i class="star-rating-icon fi-star-filled active"></i>
            </div>
            <div class="fs-xs py-2">{{ Auth::user()->gcash_number }}<br>{{ Auth::user()->email }}</div>
          </div>
        </div>
        <a href="#" class="dropdown-item">
          <i class="fi-user opacity-60 me-2"></i>
          Personal Info
        </a>
        <a href="#" class="dropdown-item">
          <i class="fi-lock opacity-60 me-2"></i>
          Password & Security
        </a>
        <a href="#" class="dropdown-item">
          <i class="fi-list opacity-60 me-2"></i>
          My Listings
        </a>
        <a href="#" class="dropdown-item">
          <i class="fi-heart opacity-60 me-2"></i>
          Wishlist
        </a>
        <a href="#" class="dropdown-item">
          <i class="fi-star opacity-60 me-2"></i>
          Reviews
        </a>
        <a href="#" class="dropdown-item">
          <i class="fi-bell opacity-60 me-2"></i>
          Notifications
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">Help</a>
        <a class="dropdown-item d-flex align-items-center" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="bi bi-box-arrow-right"></i>
          <span>Sign Out</span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>

      </div>
    </div>
    <div class="collapse navbar-collapse order-lg-2" id="navbarUserNav">
      <ul class="navbar-nav">
        <li class="nav-item {{(Route::current()->getName() === 'dashboard') ? 'active' : ''}}">
          <a href="{{ url('/') }}" class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item {{(Route::current()->getName() === 'blogs.index' || Route::current()->getName() === 'blogs.add') ? 'active' : ''}}">
          <a href="{{ route('blogs.index') }}" class="nav-link">Blog Post</a>
        </li>
       <!--  <li class="nav-item">
          <a href="#" class="nav-link">Referals <small class="text-muted">coming soon</small></a>
        </li> -->
      </ul>
    </div>
  </div>
</header>