
<nav class="mb-4" aria-label="Breadcrumb">
  <ol class="breadcrumb">
    @php $i = 0; $total = count($paths); @endphp
    {{-- @php $link = (Auth::user()->user_type == 1)? url('dashboard') : url('dashboard')  @endphp  --}}
    @php $link = url('')  @endphp
    <li class="breadcrumb-item active" aria-current="page"><a href="{{ $link }}">Dashboard</a></li>
    @foreach($paths as $key => $val)
      @php $i++;  @endphp
        @if($i == $total && $total != 1)
           <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
        @elseif($total == 1)
          <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
        @else
            <li class="breadcrumb-item"><a href="{{ $val }}">{{ $key }}</a></li>
        @endif
    @endforeach
  </ol>
</nav>


