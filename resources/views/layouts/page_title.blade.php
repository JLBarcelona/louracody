<div class="border-bottom mb-5 pt-5">
  <h1 class="mt-5 pt-4">Tooltips</h1>
  <div class="d-flex flex-wrap flex-md-nowrap justify-content-between">
    <p class="text-muted">Custom tooltips for local title storage.</p>
  </div>
</div>