<!-- Navbar with user account dropdown -->
<header class="navbar navbar-expand-lg navbar-dark bg-dark  shadow-sm fixed-top">
  <div class="container px-5">
    <a href="{{ route('website') }}" class="navbar-brand flex-shrink-0 me-2 me-xl-4">
      <img class="d-none d-lg-block d-sm-none" src="{{ asset('img/logo-nav.png') }}" width="240" alt="Finder">
      <img class="d-lg-none d-xs-block" src="{{ asset('img/logo-nav.png') }}" width="150" alt="Finder">
    </a>
    <button type="button" class="navbar-toggler ms-auto" data-bs-toggle="collapse" data-bs-target="#navbarUserNav" aria-controls="navbarUserNav" aria-expanded="false" aria-label="Toggle navigation">
     <span class="navbar-toggler-icon"></span>
    </button>

 <!--    <a href="#" target="_blank" class="btn btn-info d-none text-center d-lg-block btn-sm ms-2 order-lg-3">
      <i class="fi-facebook "></i>
    </a>
    <a href="#" target="_blank" class="btn btn-danger d-none text-center d-lg-block btn-sm ms-2 order-lg-3">
      <i class="fi-youtube "></i>
    </a> -->

    <div class="collapse navbar-collapse order-lg-2" id="navbarUserNav">
      <ul class="navbar-nav">
       <li class="nav-item {{ (Route::current()->getName() === 'website') ? 'active' : '' }}">
          <a href="{{ route('website') }}" class="nav-link">Home </a>
        </li>
       <li class="nav-item {{ (Route::current()->getName() === 'about_us') ? 'active' : '' }}">
          <a href="{{ route('about_us') }}" class="nav-link">About us</a>
        </li>
        <li class="nav-item {{ (Route::current()->getName() === 'blogs') ? 'active' : '' }}">
          <a href="{{ route('blogs') }}" class="nav-link">Blogs</a>
        </li>
         <li class="nav-item {{ (Route::current()->getName() === 'generator') ? 'active' : '' }}">
          <a href="{{ route('generator') }}" class="nav-link">Crud Generator</a>
        </li>
      </ul>
    </div>
  </div>
</header>