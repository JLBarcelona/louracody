<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @auth
    @else
        <!-- Google tag (gtag.js) -->
        <meta name="google-adsense-account" content="ca-pub-1511803601901994">

        <!-- Google tag (gtag.js) -->
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-1511803601901994" crossorigin="anonymous"></script>

        <script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=G-V254PC47SG"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-V254PC47SG');
        </script>
    @endauth
    
  
    
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <!-- SEO Meta Tags-->
    @yield('seo')
    <link rel="canonical" href="{{ url()->current() }}" />
    <meta name="author" content="{{ config('app.name') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="article:publisher" content="https://www.facebook.com/louracart" />
    
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/logo-cirle.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/logo-cirle.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/logo-cirle.png') }}">
    <link rel="manifest" href="{{ asset('site.webmanifest') }}">
    <link rel="mask-icon" color="#5bbad5" href="{{ asset('img/logo-cirle.png') }}">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles-->

    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{ asset('finder/css/theme.min.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('finder/vendor/simplebar/dist/simplebar.min.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('finder/vendor/nouislider/dist/nouislider.min.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('finder/vendor/tiny-slider/dist/tiny-slider.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('custom/sweetalert.css') }}"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.8/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">


    @stack('css')

     <!-- Page loading styles-->
    <style>
      .pointer{
        cursor: pointer;
      }
      .two-line-ellipsis {
          display: block;
          display: -webkit-box;
          overflow: hidden;
          text-overflow: ellipsis;
          -webkit-line-clamp: 2;
          -webkit-box-orient: vertical;
          text-decoration: none;
      }
      .page-loading {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        -webkit-transition: all .4s .2s ease-in-out;
        transition: all .4s .2s ease-in-out;
        background-color: #fff;
        opacity: 0;
        visibility: hidden;
        z-index: 9999;
      }
      .page-loading.active {
        opacity: 1;
        visibility: visible;
      }
      .page-loading-inner {
        position: absolute;
        top: 50%;
        left: 0;
        width: 100%;
        text-align: center;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        -webkit-transition: opacity .2s ease-in-out;
        transition: opacity .2s ease-in-out;
        opacity: 0;
      }
      .page-loading.active > .page-loading-inner {
        opacity: 1;
      }
      .page-loading-inner > span {
        display: block;
        font-size: 1rem;
        font-weight: normal;
        color: #666276;;
      }
      .page-spinner {
        display: inline-block;
        width: 2.75rem;
        height: 2.75rem;
        margin-bottom: .75rem;
        vertical-align: text-bottom;
        border: .15em solid #bbb7c5;
        border-right-color: transparent;
        border-radius: 50%;
        -webkit-animation: spinner .75s linear infinite;
        animation: spinner .75s linear infinite;
      }
      @-webkit-keyframes spinner {
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes spinner {
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      
    </style>
    <!-- Page loading scripts-->
    <script>
      (function () {
        window.onload = function () {
          var preloader = document.querySelector('.page-loading');
          preloader.classList.remove('active');
          setTimeout(function () {
            preloader.remove();
          }, 2000);
        };
      })();
      
    </script>

</head>

<body>
  <amp-auto-ads type="adsense"
          data-ad-client="ca-pub-1511803601901994">
  </amp-auto-ads>
     <!-- Page loading spinner-->
    <div class="page-loading active">
      <div class="page-loading-inner">
        <div class="page-spinner"></div><span>Loading...</span>
      </div>
    </div>

     <main class="page-wrapper">
      <!-- Navbar-->
        @auth
          @include('layouts.nav')
        @else
          @include('layouts.homenav')
           <!-- Home Nav -->
        @endauth

          @yield('landing')
          <!-- Page content-->
          <div class="container pt-5 pb-lg-4 mb-sm-2 px-sm-5 px-md-5">
            <!-- Breadcrumb-->
            @yield('breadcrumbs')

            @yield('content')
          </div>
      </main>
      @stack('modal')

      @yield('footer')

    <!-- <a class="btn-scroll-top" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon fi-chevron-up">   </i></a> -->
    <script src="{{ asset('custom/jquery.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('custom/sweetalert.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/simplebar/dist/simplebar.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/components/prism-core.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/components/prism-markup.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/components/prism-clike.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/components/prism-javascript.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/components/prism-pug.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/plugins/toolbar/prism-toolbar.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/prismjs/plugins/line-numbers/prism-line-numbers.min.js') }}"></script>
    <script src="{{ asset('finder/vendor/tiny-slider/dist/min/tiny-slider.js') }}"></script>
    <script src="https://cdn.datatables.net/1.13.8/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.8/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js"></script>    
    <script src="{{ asset('finder/js/theme.min.js') }}"></script>
    <script src="{{ asset('custom/validation.js') }}"></script>

    @stack('js')

</body>
</html>
