<aside class="offcanvas offcanvas-start offcanvas-expand bg-dark" id="componentsNav">
  <div class="offcanvas-header d-none d-lg-block pt-3 pb-4 bg-dark">
    <a class="navbar-brand py-2" href="typography.html">
      <img src="{{ asset('img/logo.png') }}" width="27" alt="Finder">
      <img src="{{ asset('img/logo-txt.png') }}" width="150" alt="Loura Cart">
    </a>
  </div>
  <div class="offcanvas-header d-flex d-lg-none align-items-center">
    <div class="d-flex align-items-center">
      <h5 class="offcanvas-title me-3 text-white">Menu</h5>
      <span class="badge bg-success">UI Kit</span>
    </div>
    <button class="btn-close btn-close-white" type="button" data-bs-dismiss="offcanvas"></button>
  </div>
  <div class="d-flex d-lg-none align-items-center py-4 px-3 border-top border-bottom border-light">
    <a class="btn btn-light btn-sm d-block w-100 me-2" href="../index.html">
      <i class="fi-eye-on me-2 fs-base"></i>Preview </a>
    <a class="btn btn-info btn-sm d-block w-100" href="../docs/dev-setup.html">
      <i class="fi-file me-2 fs-base"></i>Documentation </a>
  </div>
  <div class="offcanvas-body pt-lg-2 pb-4" data-simplebar data-simplebar-inverse>
    <h6 class="fs-base text-light mb-1 py-2">Content</h6>
    <nav class="nav nav-light flex-column">
      <a class="nav-link fs-sm" href="typography.html">Typography</a>
      <a class="nav-link fs-sm" href="icon-font.html">Icon font</a>
      <a class="nav-link fs-sm" href="code.html">Code</a>
      <a class="nav-link fs-sm" href="images.html">Images &amp; figures</a>
      <a class="nav-link fs-sm" href="tables.html">Tables</a>
    </nav>
    <hr class="text-light opacity-15 mx-n4 mt-4">
  </div>
</aside>