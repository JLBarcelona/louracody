@extends('layouts.app')

@section('title', 'Create Products')

@section('breadcrumbs')
@include('layouts.bread', ['paths' => ['Products' => route('products.index'), 'Create Product' => '']])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Create Product</div>
</div>

<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                     <form class="needs-validation" id="product_form" action="{{ route('products.save') }}/{{ $products->id ?? '' }}" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                @if(!empty($products->product_img))
                                 <img src="{{ asset($products->product_img) }}" class="img-thumbnail rounded mt-3" id="product_thumbnail_preview">
                                @else
                                 <img src="{{ asset('img/logo.jpg') }}" class="img-thumbnail rounded mt-3" id="product_thumbnail_preview">
                                @endif

                                 <input type="file" name="product_thumbnail" id="product_thumbnail" class="form-control preview-upload mt-2" data-preview-selector="product_thumbnail_preview">
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <input type="hidden" name="id" id="id" value="{{ $products->id ?? '' }}"/>
                                     <div class="col-md-12 mb-3">
                                        <label class="mb-2">Shopee link</label>
                                        <input type="text" name="shopee_link" id="shopee_link" class="form-control " required value="{{ $products->shopee_link ?? '' }}" placeholder="Enter shopee link"/>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="mb-2">Custom Link</label>
                                        <input type="text" name="custom_link" id="custom_link" class="form-control " required value="{{ $products->slug ?? '' }}" placeholder="Enter custom link"/>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="mb-2">Name</label>
                                        <input type="text" name="name" id="name" class="form-control " required value="{{ $products->name ?? '' }}" placeholder="Enter name"/>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="mb-2">Description</label>
                                        <textarea type="textarea" name="description" id="description" rows="4" class="form-control " required placeholder="Enter description">{{ $products->description ?? '' }}</textarea>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="mb-2">Shop <small class="text-muted">(optional)</small></label>
                                        <select name="shop" id="shop" class="form-select" data-value="{{ $products->shop_id ?? '' }}">
                                            <option value=""></option>
                                            @foreach($shops as $shop)
                                            <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                     <div class="col-md-12 mb-3">
                                        <label class="mb-2">Search Keyword (<strong><small>Separate with comma ','</small></strong>)</label>
                                        <textarea name="search_keyword" id="search_keyword" class="form-control" placeholder='Search keyword can help customers to find your products.
        Example: car, oil, tires.'>{{ $products->search_keyword ?? '' }}</textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-end">
                                <button type="submit" class="btn btn-success" id="product_form_btn" >Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script type="text/javascript">
  $("#product_form").on('submit', function(e) {
    e.preventDefault(); // Prevent the default form submission

    let form = $(this)[0]; // Use $(this) instead of $("#product_form")
    let data = new FormData(form);
    let url = $(this).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false, // Prevent caching of AJAX requests
        processData: false, // Prevent jQuery from processing the data
        contentType: false, // Prevent jQuery from setting contentType
        dataType: 'json',
        beforeSend: function() {
            $('#product_form_btn').prop('disabled', true);
        },
        success: function(response) {
            if (response.status == true) {
                swal("Success", response.message, "success");
                setTimeout(function() {
                    window.location = "{{ route('products.index') }}";
                }, 1500);
            } else {
                console.log(response);
            }
            validation('product_form', response.error);
            $('#product_form_btn').prop('disabled', false);
        },
        error: function(error) {
            swal("Error", "Please make sure to enter correct shopee link", "error");
            $('#product_form_btn').prop('disabled', false);
            console.log(error);
        }
    });
});


  // $("#shopee_link").on('change', function(){
  //    let shopee_link = $(this).val();
  //    $.ajax({
  //           type:"get",
  //           url:"{{ route('products.get_suggestion') }}",
  //           data:{shopee_link : shopee_link},
  //           dataType:'json',
  //           beforeSend:function(){
  //       },
  //       success:function(response){
  //           // console.log(response);
  //           if (response.status == true) {
  //               $('#search_keyword').val(response.data.keywords);
  //               $('#description').val(response.data.description);
  //               $('#name').val(response.data.productname);
  //           }else{
  //               validation('product_form', response.error);
  //               console.log(response);
  //           }
  //       },
  //       error: function(error){
  //           console.log(error);
  //       }
  //   });
  // });
</script>
@endpush