@extends('layouts.app')

@section('title', 'Products')


@section('breadcrumbs')
@include('layouts.bread', ['paths' => ['Products' => '']])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Products</div>
  <div class="col-lg-8 text-end">
     <a href="{{ route('products.add') }}" class="btn btn-primary">Add Products</a>
     <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_import">Import From Link</a>
  </div>
</div>

<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="tbl_products" style="width: 100%;"></table>
                </div>
            </div>
        </div>
    </div>
@endsection
     <!-- <a href="{{ route('products.add') }}" class="btn btn-primary">Add Products</a> -->

@push('modal')
<form id="import_product_link_form" class="needs-validation" novalidate="" action="{{ route('products.import') }}">
    @csrf
    <div class="modal" tabindex="-1" role="dialog" id="modal_import" data-bs-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Import from link</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="mb-3">Site URL</label>
                    <input type="text" name="site_url" id="site_url" class="form-control" placeholder="Enter website url of the product" required="">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-success" >Import</button>
            </div>
          </div>
        </div>
      </div>
</form>
@endpush


@push('js')
<script type="text/javascript">
$("#import_product_link_form").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let data = $(this).serialize();
    $.ajax({
        type:"POST",
        url:url,
        data:data,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
             // console.log(response);
             if (response.status == true) {
                swal("Message", response.message, 'success');
                $("#site_url").val('');
                show_products();
             }else if (response.validation == true) {
                validation('import_product_link_form', response.error);
             }else{
                swal("Error", response.message, 'error');
             }
        },
        error: function(error){
          console.log(error);
        }
      });
});


var tbl_products;
show_products();
function show_products(){
    if(tbl_products){
        tbl_products.destroy();
    }
    tbl_samples = $('#tbl_products').DataTable({
        destroy: true,
        pageLength: 10,
        responsive: true,
        ajax: "{{ route('products.list') }}",
        deferRender: true,
        fnDrawCallback: function() {
            var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
            var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
              return new bootstrap.Popover(popoverTriggerEl)
            });

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
              return new bootstrap.Tooltip(tooltipTriggerEl)
            });
        },
        columns: [
            {
                className: '',
                "data": 'name',
                "title": 'Name',
            },
            {
                className: 'text-center',
                "orderable": false,
                "data": 'description',
                "title": 'Description',
                'render': function(data){
                    return '<i class="fi-info-circle" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-trigger="hover" title="Product Description" data-bs-content="'+data+'"></i>';
                }
            },
            {
                className: 'text-center',
                "orderable": false,
                "data": 'search_keyword',
                "title": 'Keyword',
                'render': function(data){
                    return '<i class="fi-info-circle" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-trigger="hover" title="SEO Keywords" data-bs-content="'+data+'"></i>';
                }
            },
            {
                className: '',
                "data": 'site_link',
                "title": 'Shopee link',
                'render': function(data, type, row){
                    return '<span class="badge bg-accent pointer" data-bs-toggle="tooltip" title="Click to copy URL" id="site_link_'+row.id+'" onclick="copyToClipboard(\'#site_link_'+row.id+'\')">'+data+' </span> <small id="site_link_'+row.id+'_copy"></small>';
                }
            },
            {
                className: 'width-option-1 text-center',
                width: '15%',
                "data": 'id',
                "orderable": false,
                "title": 'Options',
                "render": function(data, type, row, meta){
                    newdata = '';
                    newdata += '<a href="{{ route('products.edit') }}/'+row.id+'" class="btn btn-success btn-sm font-base mt-1" ><i class="fa fa-edit"></i> Edit</a> ';
                    newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_products('+row.id+');" type="button"><i class="fa fa-trash"></i> Delete</button>';
                    return newdata;
                }
            }
        ]
    });

  

}


function delete_products(id){
    swal({
        title: "Are you sure?",
        text: "Do you want to delete products?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    },
    function(){
        $.ajax({
            type:"DELETE",
            url:"{{ route('products.delete') }}/"+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
        },
        success:function(response){
            // console.log(response);
            if (response.status == true) {
                show_products();
                swal("Success", response.message, "success");
            }else{
                console.log(response);
            }
        },
        error: function(error){
            console.log(error);
        }
        });
    });
}

</script>
@endpush