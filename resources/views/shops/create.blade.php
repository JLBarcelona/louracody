@extends('layouts.app')

@section('title', 'Create Products')

@section('breadcrumbs')
@include('layouts.bread', ['paths' => ['Shops' => route('shops.index'), 'Create Shop' => '']])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Create Shop</div>
</div>

<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                     <form class="needs-validation" id="shop_form" action="{{ route('shops.save') }}/{{ $shops->id ?? '' }}" novalidate>
                        @csrf
                        <div class="row">
                            <input type="hidden" name="id" id="id" value="{{ $shops->id ?? '' }}"/>
                            <div class="col-md-12">
                                <div class="col-md-12 mb-3">
                                    <div class="row">
                                        <div class="col-4">
                                             <img src="{{ asset('img/logo.jpg') }}" class="img-thumbnail rounded mt-3" id="shop_logo_preview">
                                             <input type="file" name="shop_logo" id="shop_logo" class="form-control preview-upload mt-2" data-preview-selector="shop_logo_preview">
                                        </div>
                                        <div class="col-8">
                                             <div class="col-md-12 mb-2">
                                                <label class="mb-2">Shopee link</label>
                                                <input type="text" name="shopee_link" id="shopee_link" class="form-control " required value="{{ $shops->shopee_link ?? '' }}" placeholder="Enter shopee link"/>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                        <label class="mb-2">Shop name</label>
                                        <input type="text" name="name" id="name" class="form-control " required value="{{ $shops->name ?? '' }}" placeholder="Enter shop name"/>
                                </div>
                                 <div class="col-md-12 mb-3">
                                <label class="mb-2">Search Keyword (<strong><small>Separate with comma ','</small></strong>)</label>
                                <textarea name="search_keyword" id="search_keyword" class="form-control" rows="6" placeholder='Search keyword can help customers to find your products.
Example: car, oil, tires.'>{{ $shops->search_keyword ?? '' }}</textarea>
                                </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-end">
                                <button type="submit" class="btn btn-primary" id="shop_form_btn" >Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script type="text/javascript">
   $("#shop_form").on('submit', function(e) {
    e.preventDefault(); // Prevent the default form submission

    let form = $(this)[0]; // Use $(this) instead of $("#shop_form")
    let data = new FormData(form);
    let url = $(this).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false, // Prevent caching of AJAX requests
        processData: false, // Prevent jQuery from processing the data
        contentType: false, // Prevent jQuery from setting contentType
        dataType: 'json',
        beforeSend: function() {
            $('#shop_form_btn').prop('disabled', true);
        },
        success: function(response) {
            if (response.status == true) {
                swal("Success", response.message, "success");
                setTimeout(function() {
                    window.location = "{{ route('shops.index') }}";
                }, 1500);
            } else {
                console.log(response);
            }
            validation('shop_form', response.error);
            $('#shop_form_btn').prop('disabled', false);
        },
        error: function(error) {
            $('#shop_form_btn').prop('disabled', false);
            console.log(error);
        }
    });
});


</script>
</script>
@endpush