@extends('layouts.app')

@section('title', 'Shops')


@section('breadcrumbs')
@include('layouts.bread', ['paths' => ['Shops' => '']])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Shops</div>
  <div class="col-lg-8 text-end">
     <a href="{{ route('shops.add') }}" class="btn btn-primary">Add Shops</a>
  </div>
</div>

<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="tbl_shops" style="width: 100%;"></table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
<script type="text/javascript">
    var tbl_shops;
    show_shops();
    function show_shops(){
        if(tbl_shops){
            tbl_shops.destroy();
        }
        tbl_samples = $('#tbl_shops').DataTable({
            destroy: true,
            pageLength: 10,
            responsive: true,
            ajax: "{{ route('shops.list') }}",
            deferRender: true,
            columns: [
                {
                    className: '',
                    "data": 'name',
                    "title": 'Shop name',
                },{
                    className: '',
                    "data": 'shopee_link',
                    "title": 'Shopee link',
                },
                {
                    className: '',
                    "data": 'lazada_link',
                    "title": 'Lazada link',
                },{
                    className: '',
                    "data": 'search_keyword',
                    "title": 'Search Keyword',
                },{
                    className: 'width-option-1 text-center',
                    width: '15%',
                    "data": 'id',
                    "orderable": false,
                    "title": 'Options',
                    "render": function(data, type, row, meta){
                        newdata = '';
                        newdata += '<a href="{{ route('shops.edit') }}/'+row.id+'" class="btn btn-success btn-sm font-base mt-1" ><i class="fa fa-edit"></i> Edit</a> ';
                        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_shops('+row.id+');" type="button"><i class="fa fa-trash"></i> Delete</button>';
                        return newdata;
                    }
                }
            ]
        });
    }


    function delete_shops(id){
        swal({
            title: "Are you sure?",
            text: "Do you want to delete shops?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type:"DELETE",
                url:"{{ route('shops.delete') }}/"+id,
                data:{},
                dataType:'json',
                beforeSend:function(){
            },
            success:function(response){
                // console.log(response);
                if (response.status == true) {
                    show_shops();
                    swal("Success", response.message, "success");
                }else{
                    console.log(response);
                }
            },
            error: function(error){
                console.log(error);
            }
            });
        });
    }
</script>
@endpush