@extends('layouts.app')

@section('seo')
<meta name="description" content="Discover how Laravelgenerator.online is reshaping the landscape of Laravel development with our intuitive online code generator. Empowering developers and students alike to streamline workflows and unleash creativity.">
<meta name="keywords" content="Laravel development, online code generator, productivity tools, collaboration, learning platform, coding education, innovation, teamwork">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Laravel Generator" />
<meta property="og:description" content="Discover how Laravelgenerator.online is reshaping the landscape of Laravel development with our intuitive online code generator. Empowering developers and students alike to streamline workflows and unleash creativity." />
<meta property="og:image" content="{{ asset('img/logo-cirle.png') }}" />

@endsection

@push('css')
<style type="text/css">
  .no-decoration{
        text-decoration: none!important;
      }
</style>
@endpush
@section('title', 'About us')

@section('landing')

  <section class="bg-light pb-4 pt-5">
    <div class="container py-5">
      <div class="row align-items-center mt-2 mt-md-0 pt-md-4 pt-lg-5 pb-5">
        <div class="col-md-12 text-center">
          <h1>About us</h1>
        </div>
        <div class="col-md-5 order-md-1 mb-5 mb-md-0">
          <img src="{{ asset('img/land2.png') }}">
        </div>
        <div class="col-md-7 order-md-2">
        <p class="text-dark"> We are passionate about revolutionizing the way developers and students approach Laravel development. With a dedicated team of experts, we have created an intuitive online code generator designed to simplify the complexities of coding and enhance productivity.</p>

        <p class="text-dark">Our mission is simple: to empower developers and students alike by providing them with powerful tools that streamline their workflow and unleash their creativity. Whether you're a seasoned professional or just starting out on your coding journey, our platform is tailored to meet your needs.</p>

        <p class="text-dark">With our collaborative approach, we believe in the power of teamwork and learning together. Our platform fosters collaboration among team members and students, allowing them to share ideas, troubleshoot problems, and accelerate their learning and development.</p>

        <p class="text-dark">We are committed to providing an exceptional user experience. We continually strive to innovate and improve our platform, ensuring that it remains at the forefront of Laravel development tools.</p>

        <p>Please support our mission to transform the way you code and learn Laravel. Experience the difference today.</p>
        </div>
      </div>
   
    </div>
  </section>

  <section class="pb-4 pt-5 ">
    <hr class="text-dark opacity-15 mx-n4 mt-4">
    <div class="container py-5 ">
      <div class="h3 text-center mb-5 text-dark text-center">DEVELOPERS</div>
        <div class="row justify-content-center">
          <div class="col-md-3">
            <div class="card">
              <img src='{{ asset("team/JLBarcelona.png") }}' class="card-img-top" alt="John Luis Barcelona" width="150">
              <div class="card-body">
                <h5 class="card-title">John Luis Barcelona</h5>
                <p class="card-text fs-sm">Senior Web Developer</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img src='{{ asset("team/teton.jpg") }}' class="card-img-top" alt="Jefferson Mark Barcelona" width="180">
              <div class="card-body">
                <h5 class="card-title">Jefferson Mark Barcelona</h5>
                <p class="card-text fs-sm">Web Developer</p>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>

@endsection

@section('content')

@endsection

@section('footer')
@include('layouts.footer')
@endsection


@push('js')
<script>

</script>
@endpush