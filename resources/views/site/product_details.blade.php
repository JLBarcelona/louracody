@extends('layouts.app')

@section('seo')
<meta name="description" content='{{ preg_replace("/\r?\n/", "", $product->description ) }}'>
<meta name="keywords" content="{{ str_replace(':', '', $product->search_keyword) }}">

@if(!empty($metas))
	@foreach($metas as $meta)
    <meta property="{{ $meta->property }}" content="{{ $meta->content }}" />
	@endforeach

    @else

    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $product->name }}" />
    <meta property="og:description" content='{{ preg_replace("/\r?\n/", "", $product->description ) }}' />
    <meta property="og:image" content="{{ asset($product->product_img) }}" />

@endif

@endsection

@section('title', $product->name)

@section('landing')
<!-- Put landing if you have -->
@endsection

@section('content')
<div class="row justify-content-center mt-5" >
    <div class="col-lg-4">
        <a class="" href="{{ route('product_link', $product->slug) }}{{ $link_extend }}" target="_blank">
            <img src="{{ asset($product->product_img) }}" class="img-fluid hover mb-2" alt="{{ $product->name }}">
        </a>
    </div>
    <div class="col-lg-8">
    	<h2>{{ $product->name }}</h2>
        <div>
            <a class="btn btn-primary mb-3 me-2" href="{{ route('product_link', $product->slug) }}{{ $link_extend }}" target="_blank"><i class="fi-cart text-white"></i> Add to cart</a>
         <!-- <h1>Reviews</h1> -->
            <h3 class="p-0 m-0">About Product</h3>
            <ul class="list-unstyled">
              <li><i class="fi-star-filled mt-n1 me-1 text-warning align-middle"></i><b>5.00 </b><span class="text-muted">(reviews)</span></li>
              <li>
                  <a class="btn btn-info btn-xs" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" target="_blank" rel="noopener noreferrer"><i class="fi-facebook text-white"></i> Share</a>
                <a class="btn btn-danger btn-xs" data-link="{{ route('product_link', $product->slug) }}" onclick="copyToClipboardLink('#copy_btn');" id="copy_btn" target="_blank" rel="noopener noreferrer"><i class="fi-paperclip text-white"></i> <span id="copy_btn_txt">Copy</span></a>
              </li>
            </ul>
            <div class=" pb-md-3">
             <p class="pb-0 mb-0">{!! nl2br($product->description) !!}</p>
             <a href="{{ route('product_link', $product->slug) }}{{ $link_extend }}" target="_blank">More Details</a>
            </div>
        </div>
    	<div class="">
    			
    	</div>
    </div>
    <!-- <div class="col-lg-12 mt-2">
        <div class="card">
            <div class="card-body">
               
            </div>
        </div>
    </div> -->
    <div class="col-lg-12 mt-4 mb-4">		
		<div class="card">
			<div class="card-body">
				<h3>Honest Reviews</h3>
			</div>
		</div>
    </div>
</div>
@endsection

@section('footer')
@include('layouts.footer')
@endsection
