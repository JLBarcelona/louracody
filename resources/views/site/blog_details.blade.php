@extends('layouts.app')

@section('seo')
<meta name="description" content="{{ $blog->meta_description }}">
<meta name="keywords" content="{{ $blog->meta_keywords }}">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Laravel Generator - {{ $blog->blog_title }}" />
<meta property="og:description" content="{{ $blog->meta_description }}" />
<meta property="og:image" content="{{ asset($blog->blog_thumbnail) }}" />

@endsection

@push('css')
<style type="text/css">
  .no-decoration{
        text-decoration: none!important;
      }
</style>
@endpush
@section('title', 'Blog Post')

@section('landing')

  <section class="bg-light pb-4 pt-5">
    <div class="container py-5">
      <div class="row row-cols-1 row-cols-md-2 gy-md-5 gy-4 mb-lg-5 mb-4">
          <article class="col-md-12 pb-2 pb-md-1"><a class="d-block position-relative mb-3" href="#"><span class="badge bg-info position-absolute top-0 end-0 m-3 fs-sm">New</span><img class="d-block rounded-3" src="{{ asset($blog->blog_thumbnail) }}" alt="Post image"></a>
             <div class="mt-1">
              <i class="fa fa-calendar-alt"></i> Posted on {{ date('Y-m-d') }}
            </div>
            <h1 class="h5 text-dark mb-2 pt-1">{{ $blog->blog_title }}</h1>
            <div>
               @php
                    $html_content = (!empty($blog->contents))? file_get_contents(asset('storage/ck_html/'.$blog->contents)) : '';
                    @endphp
                    {!! $html_content !!}
            </div>
          </article>
        </div>
    </div>
  </section>

@endsection

@section('content')

@endsection

@section('footer')
@include('layouts.footer')
@endsection


@push('js')
<script>

</script>
@endpush