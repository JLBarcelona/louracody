@extends('layouts.app')

@section('seo')
<meta name="description" content="Discover the ultimate shopping experience with Loura Cart! Our online store offers a wide selection of premium products, including fashion, electronics, home goods, and more. Shop conveniently from the comfort of your home and enjoy fast shipping, secure payments, and exceptional customer service. Explore our curated collections today and elevate your shopping game with Loura Cart!">
<meta name="keywords" content="Online shopping, E-commerce, Fashion, Electronics, Home goods, Clothing, Gadgets, Appliances, Accessories, Deals, Discounts, Quality products, Secure payments, Fast shipping, Customer satisfaction, budol deals, PC parts, good pc build">
@endsection

@section('title', 'Dashboard')

@section('landing')
<!-- Put landing if you have -->
@endsection

@section('content')
<div class="row justify-content-center mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div>
@endsection

