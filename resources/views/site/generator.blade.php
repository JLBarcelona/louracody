@extends('layouts.app')

@section('seo')
<meta name="description" content="Effortlessly create, customize, and manage CRUD (Create, Read, Update, Delete) operations in Laravel with our online CRUD generator. Generate robust code structures tailored to your specific needs, simplifying database interactions and accelerating your Laravel application development process.">

<meta name="keywords" content="Laravel code generator, online code generation tool, Laravel CRUD generator, rapid Laravel development, automated Laravel code creation, PHP framework code generator, Laravel scaffold generator, efficient Laravel code creation, custom Laravel code generation, code generation for Laravel projects, JL Barcelona, John Luis Barcelona, Laravel CRUD generator,CRUD operations in Laravel,Laravel CRUD scaffolding,Laravel CRUD builder,Online CRUD generator,Laravel CRUD generator tool,Laravel CRUD generator tutorial,Rapid CRUD development in Laravel,Laravel CRUD code generator,Laravel CRUD generator package">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Laravel Generator" />
<meta property="og:description" content="Effortlessly create, customize, and manage CRUD (Create, Read, Update, Delete) operations in Laravel with our online CRUD generator. Generate robust code structures tailored to your specific needs, simplifying database interactions and accelerating your Laravel application development process." />
<meta property="og:image" content="{{ asset('img/logo-cirle.png') }}" />

@endsection

@push('css')
<style type="text/css">
  .no-decoration{
        text-decoration: none!important;
      }
</style>
@endpush
@section('title', 'Online Laravel Crud Generator')

@section('landing')


<section class="bg-light pt-5" style="margin-top:70px;">
  <div class="container-fluid ">
    <form class="needs-validation" id="form_generate" action="{{ route('generate') }}" novalidate>
      <div class="row p-3">
        <div class="col-md-6">
          <div class="card mb-3">
            <div class="card-header"><h3>Instructions</h3></div>
            <div class="card-body pb-4">
              <p class="mt-0 mb-2">Please Download the scripts and css: <a href="{{ asset('js/validation.js') }}" download="">validation.js</a>, <a href="{{ asset('js/sweetalert.min.js') }}" download="">sweetalert.js</a> and <a href="{{ asset('css/sweetalert.css') }}" download="">sweetalert.css.</a></p>
              <br>
              <p class="mt-0 mb-2">1. Create your controller. </p>
              <p class="mt-1 mb-2">2. Enter your <b>Controller</b>, <b>Model</b>, <b>Primary Key</b> and, etc. Then start adding fields.</p>
              <p class="mt-0 mb-2">3. Click the <b>Generate</b> button to generate the code. </p>
              <p class="mt-0 mb-2">4. Copy/Paste the <b>View</b> codes to your laravel resources </p>
              <p class="mt-0 mb-2">5. Copy/Paste the <b>Route</b> codes to your laravel routes. </p>
              <p class="mt-0 mb-2">6. Copy/Paste the <b>Controller</b> codes to your laravel routes/api.php. </p>
              <p class="mt-0 mb-1">7. <b>Job Done! 🎉🎉🎉</b> </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card mb-3">
            <div class="card-header"><h3>Laravel Crud Generator </h3></div>
            <div class="card-body">
              <div class="row">
                <div class="mb-3 col-md-6">
                  <label class="form-label">Controller</label>
                  <input type="text" name="controller" value="UserController" id="controller" class="form-control">
                </div>

                <div class="mb-3 col-md-6">
                  <label class="form-label">Model</label>
                  <input type="text" name="model" id="model" value="User" class="form-control">
                </div>

                <div class="mb-3 col-md-6">
                  <label class="form-label">Prefix</label>
                  <input type="text" name="prefix" id="prefix" value="users" class="form-control">
                </div>

                <div class="mb-3 col-md-6">
                  <label class="form-label">Primary key</label>
                  <input type="text" name="primary_key" id="primary_key" value="id" class="form-control">
                </div>

                <div class="mb-3 col-md-6">
                  <label class="form-label">Form ID</label>
                  <input type="text" name="form_id" id="form_id" value="user_form" class="form-control">
                </div>
                <div class="mb-3 col-md-3">
                  <label class="form-label">Form Type</label>
                  <select name="form_type" id="form_type" class="form-select">
                    <option value="modal">Modal</option>
                    <option value="new_page">New Page</option>
                  </select>
                </div>
                <div class="mb-3 col-md-3 mb-4">
                  <label class="form-label">Bootstrap Version</label>
                  <select name="bootstrap_version" id="bootstrap_version" class="form-select">
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <table class="table table-bordered table-light table-stripped" style="width: 100%;">
            <thead>
              <tr>
                <th width="10" class="text-center">Required</th>
                <th width="180">Input Type</th>
                <th>Name/ID</th>
                <th>Class <small class="float-end my-1 mx-1">NOTE: It will apply to the input</small></th>
                <th>Placeholder/(Option)</th>
                <th width="180">Grid</th>
                <th width="30"></th>
              </tr>
            </thead>
            <tbody id="connectedSortable"></tbody>
          </table>
          <div class="mt-5 text-right">
            <button class="btn btn-primary" type="button" onclick="add_items();"><i class="fa fa-plus"></i> Add Fields</button>
            <button class="btn btn-success" type="submit"><i class="fa fa-magic"></i> Generate</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>

<div class="modal fade" role="dialog" id="modal_generated_result" data-bs-backdrop="static">
  <div class="modal-dialog modal-lg" style="max-width: 95% !important;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
          <span class="text-dark h3"><b>Generated Code</b></span></span>
        </div>
        <button class="btn-close" data-bs-dismiss="modal" onclick="on_close();"></button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="pills-view-tab" data-bs-toggle="pill" href="#pills-view" role="tab" aria-controls="pills-view" aria-selected="true">View</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-javascript-tab" data-bs-toggle="pill" href="#pills-javascript" role="tab" aria-controls="pills-javascript" aria-selected="false">Javascript</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" id="pills-model-tab" data-bs-toggle="pill" href="#pills-model" role="tab" aria-controls="pills-model" aria-selected="false">Model</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-controller-tab" data-bs-toggle="pill" href="#pills-controller" role="tab" aria-controls="pills-controller" aria-selected="false">Controller</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-route-tab" data-bs-toggle="pill" href="#pills-route" role="tab" aria-controls="pills-route" aria-selected="false">Route</a>
      </li>
    </ul>

    <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active" id="pills-view" role="tabpanel" aria-labelledby="pills-view-tab">
          
          <div class="card">
            <div class="card-header bg-dark text-white">
              <h1 class="text-white">Index</h1>
                <p><strong>Make your index blade file ex: <b>resources/views/<span class="form_prefix"></span>/index.blade.php</b> and paste this codes.</strong></p>
            </div>
            <div class="card-body bg-dark" >
              <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('view_table_text', 'copy_table')"><i class="fa fa-copy"></i> <span id="copy_table">Copy</span></button>
              <button class="btn btn-primary float-end my-1 mx-1 mr-2" onclick="download('index.blade.php', 'view_table_text')"><i class="fa fa-download"></i> <span>Download</span></button>
              <pre><code id="view_table" class="text-white"></code></pre>
              <textarea id="view_table_text" class="form-control" style="opacity: 0;"></textarea>
            </div>
          </div>

          <br>
          <br>  

          <div class="card" id="new_form_card">
            <div class="card-header bg-dark text-white">
                <h1 class="text-white">Form <span id="form_instructions"></span></h1>
                <p><strong>Make your add form blade file ex: <b>resources/views/<span class="form_prefix"></span>/add.blade.php</b> and paste this codes.</strong></p>
            </div>
            <div class="card-body bg-dark" >
              <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('view_tab_text', 'copy_view')"><i class="fa fa-copy"></i> <span id="copy_view">Copy</span></button>
              <button class="btn btn-primary float-end my-1 mx-1 mr-2" onclick="download('add.blade.php', 'view_tab_text')"><i class="fa fa-download"></i> <span>Download</span></button>

              <pre><code id="view_tab" class="text-white"></code></pre>
              <textarea id="view_tab_text" class="form-control" style="opacity: 0;"></textarea>
            </div>
          </div>        


      </div>
      <div class="tab-pane fade" id="pills-javascript" role="tabpanel"  aria-labelledby="pills-javascript-tab">
        <div class="card" id="new_form_card">
            <div class="card-header bg-dark text-white">
                <h1 class="text-white">JS for Table <span id="form_instructions"></span></h1>
                <p><strong>Put this code along with your <b>resources/views/<span class="form_prefix"></span>/index.blade.php</b> file.</strong></p>
            </div>
            <div class="card-body bg-dark" >
              <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('javascript_tab_text', 'copy_javascript')"><i class="fa fa-copy"></i> <span id="copy_javascript">Copy</span></button>
              <button class="btn btn-primary float-end my-1 mx-1 mr-2" onclick="download($('#prefix').val()+'.js', 'javascript_tab_text')"><i class="fa fa-download"></i> <span>Download</span></button>
              <pre><code id="javascript_tab" class="text-white"></code></pre>
              <textarea id="javascript_tab_text" class="form-control" style="opacity: 0;"></textarea>
            </div>
          </div>  

          <br>
          <br>  
                  
          <div class="card" id="new_form_card_js">
            <div class="card-header bg-dark text-white">
                <h1 class="text-white">JS for Form <span id="form_instructions"></span></h1>
                <p><strong>Put this code along with your <b>resources/views/<span class="form_prefix"></span>/add.blade.php</b> file.</strong></p>
            </div>
            <div class="card-body bg-dark" >
              <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('javascript_tab_form_text', 'copy_javascript_form')"><i class="fa fa-copy"></i> <span id="copy_javascript_form">Copy</span></button>
              <button class="btn btn-primary float-end my-1 mx-1 mr-2" onclick="download($('#prefix').val()+'.js', 'javascript_tab_form_text')"><i class="fa fa-download"></i> <span>Download</span></button>
              <pre><code id="javascript_tab_form" class="text-white"></code></pre>
              <textarea id="javascript_tab_form_text" class="form-control" style="opacity: 0;"></textarea>
            </div>
          </div>  


          
      </div>
      <div class="tab-pane fade bg-dark" id="pills-model" role="tabpanel"  aria-labelledby="pills-model-tab">
          <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('model_tab_text', 'copy_model')"><i class="fa fa-copy"></i> <span id="copy_model">Copy</span></button>
          <button class="btn btn-primary float-end my-1 mx-1 mr-2" onclick="download($('#model').val()+'.php', 'model_tab_text')"><i class="fa fa-download"></i> <span>Download</span></button>

        <pre><code id="model_tab" class="text-white"></code></pre>
        <textarea id="model_tab_text" class="form-control" style="opacity: 0;"></textarea>
      </div>
      <div class="tab-pane fade bg-dark" id="pills-controller" role="tabpanel"  aria-labelledby="pills-controller-tab">
          <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('controller_tab_text', 'copy_controller')"><i class="fa fa-copy"></i> <span id="copy_controller">Copy</span></button>
          <button class="btn btn-primary float-end my-1 mx-1 mr-2" onclick="download($('#controller').val()+'.php', 'controller_tab_text')"><i class="fa fa-download"></i> <span>Download</span></button>
        <pre><code id="controller_tab" class="text-white"></code></pre>
        <textarea id="controller_tab_text" class="form-control" style="opacity: 0;"></textarea>
      </div>
      <div class="tab-pane fade bg-dark" id="pills-route" role="tabpanel"  aria-labelledby="pills-route-tab">
          <button class="btn btn-primary float-end my-1 mx-1" onclick="copyClipboard('route_tab_text', 'copy_route')"><i class="fa fa-copy"></i> <span id="copy_route">Copy</span></button>
        <pre><code id="route_tab" class="text-white"></code></pre>
        <textarea id="route_tab_text" class="form-control" style="opacity: 0;"></textarea>
      </div>
    </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('content')

@endsection

@section('footer')
@include('layouts.footer')
@endsection


@push('js')
<script>

  function on_close(){
    // location.reload(); 
    
  }

  var counter = 0;
  function add_items(){
    let out = '';
    out+='<tr class="tb-data bg-white" id="inputs_'+counter+'">';
    out+='<td class="text-center"><input type="checkbox" value="1" checked name="gen['+counter+'][is_requried]"></td>';
    out+='<td>';
      out+= '<select class="form-control" name="gen['+counter+'][type]" id="type_'+counter+'">';
        out+= '<option>text</option>';
        out+= '<option>password</option>';
        out+= '<option>textarea</option>';
        out+= '<option>email</option>';
        out+= '<option>number</option>';
        out+= '<option>select</option>';
        out+= '<option>date</option>';
        out+= '<option value="datetime-local">datetime</option>';
        out+= '<option>file</option>';
        out+= '<option>readonly</option>';
        out+= '<option>hidden</option>';
        out+= '<option>tel</option>';
      out+= '</select>';
    out+='</td>';
    out+='<td><input type="text" class="form-control" oninput="auto_placeholder(this.value, \''+counter+'\')" placeholder="Enter name/id" name="gen['+counter+'][id_name]"></td>';
    out+='<td><input type="text" class="form-control" placeholder="Enter Custom class" name="gen['+counter+'][class]"></td>';
    out+='<td><input type="text" class="form-control" placeholder="Enter placeholder" id="placeholder_'+counter+'" name="gen['+counter+'][placeholder]"></td>';
    out+='<td><input type="text" class="form-control" placeholder="Ex: col-md-12" value="col-md-12" name="gen['+counter+'][grid]"></td>';
    out+='<td><button class="btn btn-danger" type="button" onclick="remove_input(\'inputs_'+counter+'\')">&times;</button></td>';
    out+='</tr>';

    $("#connectedSortable").append(out);
      $('#connectedSortable .tb-data').css('cursor', 'move');

    counter++;
  }

  function auto_placeholder(val, input){
    let type = $("#type_"+input).val();

    if (val === '') {
      $("#placeholder_"+input).val('');
    }else{
      if (type !== 'select') {
        let str = val.replace('_',' ');
        $("#placeholder_"+input).val('Enter '+str);
      }else{

      }
    }
  }

  function remove_input(id){
     swal({
          title: "Are you sure?",
          text: "Do you want to delete this item?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
          function(){
            $("#"+id).slideUp('fast',function(){
          $("#"+id).remove();
        });
        });
  }

  $("#form_generate").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let data = $(this).serialize();
    let prefix = $("#model").val();

    $.ajax({
        type:"post",
        url:url,
        data:data,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          console.log(response);
          $(".form_prefix").text(prefix);
          $("#view_tab").text(response.view);
          $("#view_tab_text").val(response.view);

          if (response.form_type == 'modal') {
            $("#new_form_card").hide();
            $("#new_form_card_js").hide();
          }else{
            $("#new_form_card").show();
            $("#new_form_card_js").show();
          }

          $("#view_table").text(response.table);
          $("#view_table_text").val(response.table);

          // table

          $("#javascript_tab").text(response.javascript);
          $("#javascript_tab_text").val(response.javascript);

          $("#javascript_tab_form").text(response.javascript_form);
          $("#javascript_tab_form_text").val(response.javascript_form);

          


          $("#model_tab").text(response.model);
          $("#model_tab_text").val(response.model);

          $("#controller_tab").text(response.controller);
          $("#controller_tab_text").val(response.controller);

          $("#route_tab").text(response.route);
          $("#route_tab_text").val(response.route);

          $("#modal_generated_result").modal('show');
        },
        error: function(error){
          console.log(error);
        }
      });
  });

  $("#connectedSortable").sortable();



  function copyClipboard(element, btn) {
        var copyText = document.getElementById(element);
        copyText.select();
        document.execCommand('copy');
        $("#"+btn).text('Copied');
        setTimeout(function(){
          $("#"+btn).text('Copy');
        },1400);
      }


   function download(filename, ele) {
    let text = $("#"+ele).val();
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
</script>
@endpush