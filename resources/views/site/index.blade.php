@extends('layouts.app')

@section('seo')
<meta name="description" content="Generate Laravel code swiftly and effortlessly with our online code generator tool. Simplify your development process by automating repetitive tasks and scaffolding boilerplate code for your Laravel projects. Whether you need to create CRUD operations, models, controllers, or views, our tool streamlines the process, allowing you to focus more on building innovative features and less on manual coding. Try it now and supercharge your Laravel development workflow. Developed John Luis Barcelona also known as JL Barcelona">

<meta name="keywords" content="Laravel code generator, online code generation tool, Laravel CRUD generator, rapid Laravel development, automated Laravel code creation, PHP framework code generator, Laravel scaffold generator, efficient Laravel code creation, custom Laravel code generation, code generation for Laravel projects, JL Barcelona, John Luis Barcelona, Laravel CRUD generator,CRUD operations in Laravel,Laravel CRUD scaffolding,Laravel CRUD builder,Online CRUD generator,Laravel CRUD generator tool,Laravel CRUD generator tutorial,Rapid CRUD development in Laravel,Laravel CRUD code generator,Laravel CRUD generator package">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Laravel Generator" />
<meta property="og:description" content="Generate Laravel code swiftly and effortlessly with our online code generator tool. Simplify your development process by automating repetitive tasks and scaffolding boilerplate code for your Laravel projects. Whether you need to create CRUD operations, models, controllers, or views, our tool streamlines the process, allowing you to focus more on building innovative features and less on manual coding. Try it now and supercharge your Laravel development workflow." />
<meta property="og:image" content="{{ asset('img/logo-cirle.png') }}" />

@endsection

@push('css')
<style type="text/css">
  .no-decoration{
        text-decoration: none!important;
      }
</style>
@endpush
@section('title', 'Online Laravel Crud Generator')

@section('landing')
<section class="bg-dark pb-4 pt-5">
    <div class="container py-5">
      <div class="row align-items-center mt-2 mt-md-0 pt-md-4 pt-lg-5 pb-5">
        <div class="col-md-5 order-md-2 mb-5 mb-md-0">
          <img src="{{ asset('img/land.png') }}">
        </div>
        <div class="col-md-7 order-md-1">
          <h1 class="display-4 text-light pb-2 mb-2 mb-lg-2" style="max-width: 29.5rem;">Boost your  <span class="text-primary">Laravel Development</span></h1>
          <h2 class="h5 text-light mb-4">Our online code generator simplifies tasks, automates CRUD operations, models, controllers, and views. Focus on innovation, not coding. Try now for a faster workflow for free.</h2>
          <a href="{{ route('generator') }}" class="btn btn-primary">Generate CRUD for free</a>
        </div>
      </div>
   
    </div>
  </section>
  

  <section class="bg-light pb-4 pt-5">
    <div class="container py-5">
      <div class="row align-items-center mt-2 mt-md-0 pt-md-4 pt-lg-5 pb-5">
        <div class="col-md-5 order-md-1 mb-5 mb-md-0">
          <img src="{{ asset('img/land2.png') }}">
        </div>
        <div class="col-md-7 order-md-2">
          <h1 class="display-4 text-light pb-2 mb-2 mb-lg-2" style="max-width: 49.5rem;"><span class="text-primary">Why CRUD Generator?</span></h1>
          <h2 class="h5 text-dark mb-4">Empower your team or class with our online code generator, perfect for both professionals and students. automate CRUD operations, models, controllers, and views effortlessly. Unlock innovation without the hassle of manual coding. Elevate your learning journey or professional projects together. Discover the difference now. Give it a try!</h2>
          <a href="{{ route('generator') }}" class="btn btn-primary">Try it</a>
        </div>
        <div class="col-md-12 order-md-3 mt-3">
           <h1>Experience the Efficiency of Our Laravel CRUD Generator</h1>
          <p>Revolutionize your development process with our seamless automation of CRUD operations, models, controllers, and views. Say goodbye to manual coding and hello to innovation.</p>

          <h2>Why Choose Our Tool?</h2>
          <p>Our Laravel CRUD Generator is designed to save you valuable time and effort by automating repetitive CRUD tasks, allowing you to accelerate your development cycle and deliver projects faster. Additionally, it promotes consistency and reliability in your codebase by standardizing the structure of your Laravel applications, resulting in cleaner, more maintainable code. Whether you're a professional looking to streamline your workflows or a student seeking a hands-on learning experience, our tool is perfect for you.</p>

          <h2>Unleash Your Creativity</h2>
          <p>With our Laravel CRUD Generator, you can focus on unleashing your creativity and taking your projects to new heights. Don't let manual coding hold you back – try it now and experience the difference firsthand!</p>

        </div>
      </div>
   
    </div>
  </section>

<section class="pb-4 pt-5 bg-dark">
    <div class="container py-5">
      <h2 class="h3 text-center  mb-5 text-light">Tool's Compatibility</h2>
        <div class="tns-carousel-wrapper tns-nav-outside tns-nav-outside-flush">
          <div class="tns-outer" id="tns2-ow">
            <div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span class="current">11 to 15</span> of 6 </div>
            <div id="tns2-mw" class="tns-ovh">
              <div class="tns-inner" id="tns2-iw">
                <div class="tns-carousel-inner  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" data-carousel-options="{&quot;controls&quot;: false, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2},&quot;480&quot;:{&quot;items&quot;:3},&quot;680&quot;:{&quot;items&quot;:4, &quot;gutter&quot;: 12},&quot;850&quot;:{&quot;items&quot;:5, &quot;gutter&quot;: 16},&quot;1100&quot;:{&quot;items&quot;:6, &quot;gutter&quot;: 24}}}" id="tns2" style="transition-duration: 0s; transform: translate3d(-38.4615%, 0px, 0px);">
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/laravel.png') }}" width="80" alt="Laravel">
                  </div>
                   <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/php.png') }}" width="80" alt="PHP">
                  </div>
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/bs.png') }}" width="80" alt="Bootstrap 5">
                  </div>
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/mysql.png') }}" width="80" alt="mysql">
                  </div>
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/html.png') }}" width="80" alt="Html 5">
                  </div>
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/jquery.png') }}" width="80" alt="jquery">
                  </div>
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/css.png') }}" width="80" alt="CSS 3">
                  </div>
                  <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/js.png') }}" width="80" alt="javascript">
                  </div>
                   <div class="pb-3 text-center tns-item tns-slide-cloned" aria-hidden="true" tabindex="-1">
                      <img class="swap-from" src="{{ asset('icons/swal.png') }}" width="80" alt="Sweetalert">
                  </div>
                </div>
              </div>
            </div>
          
          </div>
        </div>
    </div>
</section>

<section class="pb-4 pt-5 ">
    <div class="container py-5 ">
      <div class="h3 text-center mb-5 text-dark text-center">DEVELOPERS</div>
        <div class="row justify-content-center">
          <div class="col-md-3">
            <div class="card">
              <img src='{{ asset("team/JLBarcelona.png") }}' class="card-img-top" alt="John Luis Barcelona" width="150">
              <div class="card-body">
                <h5 class="card-title">John Luis Barcelona</h5>
                <p class="card-text fs-sm">Senior Web Developer</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card">
              <img src='{{ asset("team/teton.jpg") }}' class="card-img-top" alt="Jefferson Mark Barcelona" width="180">
              <div class="card-body">
                <h5 class="card-title">Jefferson Mark Barcelona</h5>
                <p class="card-text fs-sm">Web Developer</p>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>



@endsection

@section('content')

@endsection

@section('footer')
@include('layouts.footer')
@endsection


@push('js')
<script>
<script type="text/javascript">

  function on_close(){
    // location.reload(); 
    
  }

  var counter = 0;
  function add_items(){
    let out = '';
    out+='<tr class="tb-data" id="inputs_'+counter+'">';
    out+='<td class="text-center"><input type="checkbox" value="1" checked name="gen['+counter+'][is_requried]"></td>';
    out+='<td>';
      out+= '<select class="form-control" name="gen['+counter+'][type]" id="type_'+counter+'">';
        out+= '<option>text</option>';
        out+= '<option>password</option>';
        out+= '<option>textarea</option>';
        out+= '<option>email</option>';
        out+= '<option>number</option>';
        out+= '<option>select</option>';
        out+= '<option>date</option>';
        out+= '<option value="datetime-local">datetime</option>';
        out+= '<option>file</option>';
        out+= '<option>readonly</option>';
        out+= '<option>hidden</option>';
        out+= '<option>tel</option>';
      out+= '</select>';
    out+='</td>';
    out+='<td><input type="text" class="form-control" oninput="auto_placeholder(this.value, \''+counter+'\')" placeholder="Enter name/id" name="gen['+counter+'][id_name]"></td>';
    out+='<td><input type="text" class="form-control" placeholder="Enter Custom class" name="gen['+counter+'][class]"></td>';
    out+='<td><input type="text" class="form-control" placeholder="Enter placeholder" id="placeholder_'+counter+'" name="gen['+counter+'][placeholder]"></td>';
    out+='<td><input type="text" class="form-control" placeholder="Ex: col-md-12" value="col-md-12" name="gen['+counter+'][grid]"></td>';
    out+='<td><button class="btn btn-danger" type="button" onclick="remove_input(\'inputs_'+counter+'\')"><i class="fa fa-times"></i></button></td>';
    out+='</tr>';

    $("#connectedSortable").append(out);
      $('#connectedSortable .tb-data').css('cursor', 'move');

    counter++;
  }

  function auto_placeholder(val, input){
    let type = $("#type_"+input).val();

    if (val === '') {
      $("#placeholder_"+input).val('');
    }else{
      if (type !== 'select') {
        let str = val.replace('_',' ');
        $("#placeholder_"+input).val('Enter '+str);
      }else{

      }
    }
  }

  function remove_input(id){
     swal({
          title: "Are you sure?",
          text: "Do you want to delete this item?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
          function(){
            $("#"+id).slideUp('fast',function(){
          $("#"+id).remove();
        });
        });
  }

  $("#form_generate").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let data = $(this).serialize();

    $.ajax({
        type:"post",
        url:url,
        data:data,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          console.log(response);
          
          $("#view_tab").text(response.view);
          $("#view_tab_text").val(response.view);

          if (response.form_type == 'modal') {
            $("#new_form_card").hide();
            $("#new_form_card_js").hide();
          }else{
            $("#new_form_card").show();
            $("#new_form_card_js").show();
          }

          $("#view_table").text(response.table);
          $("#view_table_text").val(response.table);

          // table

          $("#javascript_tab").text(response.javascript);
          $("#javascript_tab_text").val(response.javascript);

          $("#javascript_tab_form").text(response.javascript_form);
          $("#javascript_tab_form_text").val(response.javascript_form);

          


          $("#model_tab").text(response.model);
          $("#model_tab_text").val(response.model);

          $("#controller_tab").text(response.controller);
          $("#controller_tab_text").val(response.controller);

          $("#route_tab").text(response.route);
          $("#route_tab_text").val(response.route);

          $("#modal_generated_result").modal('show');
        },
        error: function(error){
          console.log(error);
        }
      });
  });

  $("#connectedSortable").sortable();



  function copyClipboard(element, btn) {
        var copyText = document.getElementById(element);
        copyText.select();
        document.execCommand('copy');
        $("#"+btn).text('Copied');
        setTimeout(function(){
          $("#"+btn).text('Copy');
        },1400);
      }


   function download(filename, ele) {
    let text = $("#"+ele).val();
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
</script>
</script>
@endpush