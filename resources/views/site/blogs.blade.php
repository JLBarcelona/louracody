@extends('layouts.app')

@section('seo')
<meta name="description" content="Discover a comprehensive list of Laravel online CRUD generators, empowering developers with efficient tools for rapid application development. Explore top-rated solutions tailored to streamline database operations and accelerate web development workflows effortlessly.">
<meta name="keywords" content="Laravel CRUD generator,CRUD operations in Laravel,Laravel CRUD scaffolding,Laravel CRUD builder,Online CRUD generator,Laravel CRUD generator tool,Laravel CRUD generator tutorial,Rapid CRUD development in Laravel,Laravel CRUD code generator,Laravel CRUD generator package">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Laravel Generator" />
<meta property="og:description" content="Discover a comprehensive list of Laravel online CRUD generators, empowering developers with efficient tools for rapid application development. Explore top-rated solutions tailored to streamline database operations and accelerate web development workflows effortlessly." />
<meta property="og:image" content="{{ asset('img/logo-cirle.png') }}" />

@endsection

@push('css')
<style type="text/css">
  .no-decoration{
        text-decoration: none!important;
      }
</style>
@endpush
@section('title', 'Blog Post')

@section('landing')

  <section class="bg-light pb-4 pt-5">
    <div class="container py-5">
      <div class="row row-cols-1 row-cols-md-2 gy-md-5 gy-4 mb-lg-5 mb-4">
          
          @forelse($blogs as $blog)
          <article class="col pb-2 pb-md-1"><a class="d-block position-relative mb-3" href="{{ route('blogs_details', $blog->blog_title) }}"><span class="badge bg-info position-absolute top-0 end-0 m-3 fs-sm">New</span><img class="d-block rounded-3" src="{{ asset($blog->blog_thumbnail) }}" alt="Post image"></a>
             <div class="mt-1">
              <i class="fa fa-calendar-alt"></i> Posted on {{ date('Y-m-d') }}
            </div>
            <h3 class="h5 text-dark mb-2 pt-1"><a class="nav-link" href="{{ route('blogs_details', $blog->blog_title) }}">{{ $blog->blog_title }}</a></h3>
            <p class="text-dark opacity-90 mb-3">{{ $blog->meta_description }}</p>
          </article>
          @empty
          <div class="col-md-12 mt-4">
            <h1>No Blog posts available!</h1>
          </div>
          @endforelse
        </div>
    </div>
  </section>

@endsection

@section('content')

@endsection

@section('footer')
@include('layouts.footer')
@endsection


@push('js')
<script>

</script>
@endpush