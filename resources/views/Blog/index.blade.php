@extends('layouts.app')

@section('title', 'Shops')


@section('breadcrumbs')
@include('layouts.bread', ['paths' => ['Blogs' => '']])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Blogs Management</div>
  <div class="col-lg-8 text-end">
     <a href="{{ route('blogs.add') }}" class="btn btn-primary">Add Blog Post</a>
  </div>
</div>

<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="tbl_blogs" style="width: 100%;"></table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
<script type="text/javascript">
  var tbl_blogs;
show_blogs();
function show_blogs(){
    if(tbl_blogs){
        tbl_blogs.destroy();
    }
    tbl_samples = $('#tbl_blogs').DataTable({
        destroy: true,
        pageLength: 10,
        responsive: true,
        ajax: "{{ route('blogs.list') }}",
        deferRender: true,
        columns: [
            {
                className: '',
                "data": 'blog_title',
                "title": 'Blog title',
            },
            {
                className: '',
                "data": 'contents',
                "title": 'Contents',
            },
            {
                className: '',
                "data": 'meta_description',
                "title": 'Meta description',
            },
            {
                className: '',
                "data": 'meta_keywords',
                "title": 'Meta keyword',
            },
            {
                className: 'width-option-1 text-center',
                width: '15%',
                "data": 'id',
                "orderable": false,
                "title": 'Options',
                "render": function(data, type, row, meta){
                    newdata = '';
                    newdata += '<a href="{{ route('blogs.edit') }}/'+row.id+'" class="btn btn-success btn-sm font-base mt-1" ><i class="fa fa-edit"></i></a> ';
                    newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_blogs('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
                    return newdata;
                }
            }
        ]
    });
}


function delete_blogs(id){
    swal({
        title: "Are you sure?",
        text: "Do you want to delete blogs?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    },
    function(){
        $.ajax({
            type:"DELETE",
            url:"{{ route('blogs.delete') }}/"+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
        },
        success:function(response){
            // console.log(response);
            if (response.status == true) {
                show_blogs();
                swal("Success", response.message, "success");
            }else{
                console.log(response);
            }
        },
        error: function(error){
            console.log(error);
        }
        });
    });
}

</script>
@endpush