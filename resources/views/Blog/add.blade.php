@extends('layouts.app')

@section('title', 'Create Products')

@section('breadcrumbs')
@include('layouts.bread', ['paths' => ['Blogs' => route('blogs.index'), 'Create Blog Post' => '']])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Create Blog</div>
</div>

<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="needs-validation" method="post" id="blog_form" action="{{ route('blogs.save') }}/{{ $blogs->id ?? '' }}" novalidate>
                        @csrf
                        <div class="row">
                           <input type="hidden" name="id" id="id" value="{{ $blogs->id ?? '' }}"/>
                           <div class="col-12 mb-3">
                                 @if(!empty($blogs->blog_thumbnail))
                                 <img src="{{ asset($blogs->blog_thumbnail) }}" class="img-thumbnail rounded mt-3" id="blog_thumbnail_preview" onclick="$('#blog_thumbnail').click();" style="cursor: pointer;">
                                @else
                                 <img src="{{ asset('img/thumb2.jpg') }}" class="img-thumbnail rounded mt-3" id="blog_thumbnail_preview" onclick="$('#blog_thumbnail').click();" style="cursor: pointer;">
                                @endif
                                 <input type="file" name="blog_thumbnail" id="blog_thumbnail" class="form-control preview-upload mt-2" data-preview-selector="blog_thumbnail_preview">
                                
                            </div>
                            <div class="my-3">
                                <hr>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="mb-2">Blog title</label>
                                <input type="text" name="blog_title" id="blog_title" class="form-control " required value="{{ $blogs->blog_title ?? '' }}" placeholder="Enter blog title"/>
                            </div>
                             @php
                                $html_content = (!empty($blogs->contents))? file_get_contents(asset('storage/ck_html/'.$blogs->contents)) : '';
                            @endphp
                            <div class="col-md-12 mb-3">
                                <label class="mb-2">Contents</label>
                                <textarea id="contents_html" name="contents_html" class="d-none">{!! $html_content !!}</textarea>
                                <input type="hidden" id="contents" name="contents" class="form-control">
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="mb-2">Meta description</label>
                                <textarea type="textarea" name="meta_description" id="meta_description" class="form-control " required placeholder="Enter meta description">{{ $blogs->meta_description ?? '' }}</textarea>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="mb-2">Meta keyword</label>
                                <textarea type="textarea" name="meta_keyword" id="meta_keyword" class="form-control " required placeholder="Enter meta keyword">{{ $blogs->meta_keywords ?? '' }}</textarea>
                            </div>
                            <div class="col-md-12 text-end">
                                <button type="submit" class="btn btn-success" id="blog_form_btn" >Save</button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">
 $(document).ready(function(){
    CKEDITOR.replace('contents_html', {
      extraPlugins: 'uploadimage',
        filebrowserUploadUrl : "{{ route('ckeditor.upload', ['_token' => CSRF_TOKEN()]) }}",
      height: 1000,
    }).config.allowedContent = true;
  });

$("#blog_form").on('submit', function(e){
    e.preventDefault();
    $('#contents').val(CKEDITOR.instances['contents_html'].getData());
    let form = $(this)[0]; // Use $(this) instead of $("#product_form")
    let data = new FormData(form);
    let url = $(this).attr('action');

    $.ajax({
      type:"POST",
      url:url,
      data:data,
      enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
      dataType:'json',
        beforeSend:function(){
            $('#blog_form').prop('disabled', true);
        },
        success:function(response){
            // console.log(response);
            if (response.status == true) {
                swal("Success", response.message, "success");
                setTimeout(function(){
                    window.location = "{{ route('blogs.index') }}";
                }, 1500);
            }else{
                console.log(response);
            }
                validation('blogs_form', response.error);
                $('#blog_form').prop('disabled', false);
        },
        error: function(error){
            $('#blog_form').prop('disabled', false);
            console.log(error);
        }
    });
});

</script>
</script>
@endpush