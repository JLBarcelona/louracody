@extends('layouts.app')

@section('seo')
<meta name="description" content="Finder - Directory &amp; Listings Bootstrap Template">
<meta name="keywords" content="bootstrap, business, directory, listings, e-commerce, car dealer, city guide, real estate, job board, user account, multipurpose, ui kit, html5, css3, javascript, gallery, slider, touch">
@endsection

@section('title', 'Dashboard')

@section('breadcrumbs')
@include('layouts.bread', ['title' => 'Dashboard', 'paths' => []])
@endsection

@section('content')
<div class="border-bottom mb-5 pb-3 row">
  <div class="col-lg-4 h1">Dashboard</div>
</div>

<div class="row justify-content-center">
    <div class="col-md-3 mb-3">
        <div class="card text-dark bg-white card-hover float">
          <div class="card-body">
            <h5 class="card-title text-dark">Views</h5>
            <p class="card-text fs-sm">{{ $v_count ?? 0 }}</p>
          </div>
        </div>
    </div>
    <div class="col-md-3 mb-3">
        <div class="card text-dark bg-white card-hover float">
          <div class="card-body">
            <h5 class="card-title text-dark">Products </h5>
            <p class="card-text fs-sm">{{ $p_count ?? 0 }}</p>
          </div>
        </div>
    </div>

    <div class="col-md-3 mb-3">
        <div class="card text-dark bg-white card-hover float">
          <div class="card-body">
            <h5 class="card-title text-dark">Shops</h5>
            <p class="card-text fs-sm">{{ $s_count ?? 0 }}</p>
          </div>
        </div>
    </div>

    <div class="col-md-3 mb-3">
        <div class="card text-dark bg-white card-hover float">
          <div class="card-body">
            <h5 class="card-title text-dark">Pendings</h5>
            <p class="card-text fs-sm">0</p>
          </div>
        </div>
    </div>
</div>
@endsection
