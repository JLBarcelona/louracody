<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	use HasFactory;

	protected $fillable = [
		'blog_title',
		'contents',
		'meta_description',
		'meta_keyword',
	];
}