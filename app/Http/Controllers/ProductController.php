<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Shop;
use Validator;
use Str;

use Illuminate\Support\Facades\Http;
use App\Services\InvolveService;
use App\Services\ContentCrawlService;

class ProductController extends Controller
{


	public function __construct()
    {
        $this->middleware('auth');
    }


    public function import(Request $request){
    	$validator = Validator::make($request->all(), [
			'site_url' => 'required|url|unique:products,other_links',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'validation' => true, 'error' => $validator->errors() ]);
		}else{
			$crawl = new ContentCrawlService();
			$gen = $crawl->generate($request->get('site_url'));

			$involve = new InvolveService();
			$inv_link = $involve->generate($gen['shopee_url']);

			if ($gen['status']) {
				$products = new Product;
				$products->slug =  $gen['slug'];
				$products->name =  $gen['product_name'];
				$products->description =  $gen['description'];
				$products->shopee_link =  $gen['shopee_url'];
				$products->other_links = $request->get('site_url');
				$shop_owner = Shop::where('name','LIKE','%'.'Rs8'.'%');
				if ($shop_owner->count() > 0) {
					$shop_owner_f = $shop_owner->first();
					$products->shop_id =  $shop_owner_f->id;
				}
				$products->involve_link_shopee = $inv_link;
				$products->search_keyword = $gen['search_keyword'];
				$products->meta_code =  $gen['meta_codes'];
				$products->product_img = $gen['img'];
				$products->save();

				if ($products->save()) {
					return response()->json(['status' => true, 'validation' => false, 'message' => 'Product imported successfully!' ]);
				}

			}else{
					return response()->json(['status' => false, 'validation' => false, 'message' => 'There\'s Something wrong with the API' ]);
			}
		}
    }

    public function getInvolveAsia(){
		$involve = new InvolveService();
		return $involve->offers();

    }

	public function index(){
		return view('products.index');
	}

	public function getSuggestions(Request $request){
		$validator = Validator::make($request->all(), [
			'shopee_link' => 'required|url',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			return response()->json(['status' => true, 'data' => $this->extractProductDetails($request->shopee_link)]);
		}

	}

	public function list(){
		$products = Product::get();
		return response()->json(['status' => true, 'data' => $products ]);
	}

	public function save(Request $request, $id = ""){

		$is__require = (empty($id))? 'required|file|max:10240|mimes:jpeg,png' : '';
		$validator = Validator::make($request->all(), [
			'product_thumbnail' => $is__require,
			'name' => 'required',
			'description' => 'required',
			'shopee_link' => 'required|url|unique:products,shopee_link,'.$id,
			'custom_link' => 'unique:products,slug,'.$id,
			'search_keyword' => 'required'
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$products = Product::find($id);
				$products->name =  $request->get('name');

				if (!empty($request->get('custom_link'))) {
					$products->slug = str_replace(['/', '.', ' ', '%', '\\', '\''], ['-', '-', '-', '', '-', ''], $request->get('custom_link'));
				}else{
					if (empty($products->slug)) {
						$products->slug = $this->getMaxID();
					}
				}

				

				if (empty($products->involve_link_shopee) || $products->shopee_link !== $request->get('shopee_link')) {	
					$involve = new InvolveService();
					$inv_link = $involve->generate($request->get('shopee_link'));
					$products->involve_link_shopee = $inv_link;
				}

				$products->description =  $request->get('description');
				$products->shopee_link =  $request->get('shopee_link');
				$products->shop_id =  $request->get('shop');
				$products->search_keyword =  str_replace(' ', ',', $request->get('search_keyword'));
				if ($request->hasFile('product_thumbnail') && $request->file('product_thumbnail')->isValid()) {
			        $products->product_img = 'storage/'.$request->file('product_thumbnail')->store('product_thumbnail', 'public');
			    }

				if($products->save()){
					return response()->json(['status' => true, 'message' => 'products saved successfully!']);
				}
			}else{
				$involve = new InvolveService();
				$inv_link = $involve->generate($request->get('shopee_link'));

				$products = new Product;
				if (!empty($request->get('custom_link'))) {
					$products->slug = str_replace(['/', '.', ' ', '%', '\\', '\''], ['-', '-', '-', '', '-', ''], $request->get('custom_link'));
				}else{
					if (empty($products->slug)) {
						$products->slug = $this->getMaxID();
					}
				}

				$products->name =  $request->get('name');
				$products->description =  $request->get('description');
				$products->shopee_link =  $request->get('shopee_link');
				$products->shop_id =  $request->get('shop');
				$products->involve_link_shopee = $inv_link;
				$products->search_keyword =  str_replace(' ', ',', $request->get('search_keyword'));
				if ($request->hasFile('product_thumbnail') && $request->file('product_thumbnail')->isValid()) {
			        $products->product_img = 'storage/'.$request->file('product_thumbnail')->store('product_thumbnail', 'public');
			    }
				if($products->save()){
					return response()->json(['status' => true, 'message' => 'products updated successfully!']);
				}
			}
		}
	}

	public function getMaxID(){
		$max = Product::max('id');
		return Str::random(3).$max.Str::random(5);
	}

	public function edit($id){
		$title = 'Edit Products';
		$shops = Shop::get();
		$products = Product::findOrFail($id);
		return view('products.create', compact('products', 'title', 'shops'));
	}

	public function add(){
		$shops = Shop::get();
		$title = 'Add Products';
		return view('products.create', compact('title', 'shops'));
	}

	public function find($id){
		$products = Product::findOrFail($id);
		return response()->json(['status' => true, 'data' => $products ]);
	}

	public function delete($id){
		$products = Product::findOrFail($id);
		if($products->delete()){
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

	public function extractProductDetails($productUrl)
    {
        // Get the product URL from the request
        // $productUrl = "https://shopee.ph/DRIVER'S-LICENSE-AND-OR-CR-HOLDER-WITH-RFID-SLOTS-(GORDON-MATERIAL-WITH-EMBOSSED-LOGO)-i.104084527.25950384689?publish_id=&sp_atk=47c3c0ca-017f-45c2-944f-01dbba0726d5&xptdk=47c3c0ca-017f-45c2-944f-01dbba0726d5";

        

        $extractKeywords = $this->extractKeywords($productUrl);
        $extractProductName = $this->extractProductName($productUrl);
        $productName = $this->extractProductNameOrginal($productUrl);
        return ['keywords' => urldecode($extractKeywords), 'description' => urldecode($productName), 'productname' => urldecode($productName)];
    }

    private function extractProductName($url)
    {
        // Extract the path component of the URL
        $path = parse_url($url, PHP_URL_PATH);

        // Get the last segment of the path, which should contain the product name
        $productName = Str::slug(last(explode('/', $path)));

        // Replace dashes with spaces
        $productName = str_replace('-', ' ', $productName);


         $encodedProductName = htmlspecialchars($productName, ENT_QUOTES, 'UTF-8');


        return $productName;
    }


    private function extractKeywords($url)
    {
    
        // Extract the product name from the URL
        $productName = $this->extractProductName($url);

        // Split the product name into key phrases
        $keyPhrases = $this->splitIntoKeyPhrases($productName);

        // Refine and organize the key phrases (you can add more logic here)
        $keywords = $this->refineKeywords($keyPhrases);

        // Return the extracted keywords
        return $keywords;
    }

    private function extractProductNameOrginal($url){
        // Get the URL from the request
        // $url = $request->url();
        
        // Parse the URL
        $parsedUrl = parse_url($url);
        
        // Get the path from the parsed URL
        $path = $parsedUrl['path'];
        
        // Split the path by "/"
        $pathParts = explode("/", $path);
        
        // The product name is usually the last part of the path
        $productName = end($pathParts);
        
        // Output or use the product name as needed
        return $productName;
    }

    private function splitIntoKeyPhrases($productName)
    {
        // Split the product name into key phrases based on spaces
        return explode(' ', $productName);
    }

    private function refineKeywords($keyPhrases)
    {
        // Here you can add more logic to refine and organize the keywords
        // For example, you could remove stop words, filter out irrelevant terms, or combine related phrases

        return implode(', ', $keyPhrases);
    }

}