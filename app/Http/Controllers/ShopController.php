<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Shop;
use Validator;

class ShopController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
		return view('shops.index');
	}

	public function list(){
		$shops = Shop::get();
		return response()->json(['status' => true, 'data' => $shops ]);
	}

	public function save(Request $request, $id = ""){

		$is__require = (empty($id))? 'required|file|max:10240|mimes:jpeg,png' : '';
		$validator = Validator::make($request->all(), [
			'shop_logo' => $is__require,
			'name' => 'required',
			'shopee_link' => 'required',
			'search_keyword' => 'required',
		]);

        
	
		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$shops = Shop::find($id);
				$shops->name = $request->get('name');
				$shops->shopee_link = $request->get('shopee_link');
				$shops->lazada_link = $request->get('lazada_link');
				$shops->search_keyword = $request->get('search_keyword');
				if ($request->hasFile('shop_logo') && $request->file('shop_logo')->isValid()) {
			        $shops->shop_img = $request->file('shop_logo')->store('shop_logo', 'public');
			    }
				$shops->save();
				return response()->json(['status' => true, 'message' => 'shops updated successfully!']);
			}else{
				$shops = new Shop;
				$shops->name = $request->get('name');
				$shops->shopee_link = $request->get('shopee_link');
				$shops->lazada_link = $request->get('lazada_link');
				$shops->search_keyword = $request->get('search_keyword');
				if ($request->hasFile('shop_logo') && $request->file('shop_logo')->isValid()) {
			        $shops->shop_img = $request->file('shop_logo')->store('shop_logo', 'public');
			    }
				$shops->save();
				return response()->json(['status' => true, 'message' => 'shops updated successfully!']);
			}
		}
	}

	public function edit($id){
		$title = 'Edit Shops';
		$shops = Shop::findOrFail($id);
		return view('shops.create', compact('shops', 'title'));
	}

	public function add(){
		$title = 'Add Shops';
		return view('shops.create', compact('title'));
	}

	public function find($id){
		$shops = Shop::findOrFail($id);
		return response()->json(['status' => true, 'data' => $shops ]);
	}

	public function delete($id){
		$shops = Shop::findOrFail($id);
		if($shops->delete()){
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}