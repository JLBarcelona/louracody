<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Blog;
use Validator;
use File;
use Str;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
	public function index(){
		return view('Blog.index');
	}

	public function list(){
		$blogs = Blog::get();
		return response()->json(['status' => true, 'data' => $blogs ]);
	}


	function makeCKContent($filename, $ck_contents){
	    Storage::disk('ck')->put($filename, $ck_contents);
	    return $filename;

	}

	public function save(Request $request, $id = ""){
			$is__require = (empty($id))? 'required|file|max:10240|mimes:jpeg,png,JPEG,jpg,GIF,gif,web' : '';

		$validator = Validator::make($request->all(), [
			'blog_thumbnail' => $is__require,
			'blog_title' => 'required',
			'contents' => 'required',
			'meta_description' => 'required',
			'meta_keyword' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			

			if(!empty($id)){
				$blogs = Blog::find($id);

				$filename = $blogs->contents ?? 'blogs_'.Str::random(8).'.txt'; 
                $ck = $this->makeCKContent($filename, $request->contents);

				$blogs->blog_title = $request->get('blog_title');
				$blogs->contents = $ck;
				$blogs->meta_description = $request->get('meta_description');
				$blogs->meta_keywords = $request->get('meta_keyword');
				
				if ($request->hasFile('blog_thumbnail') && $request->file('blog_thumbnail')->isValid()) {
			        $blogs->blog_thumbnail = 'storage/'.$request->file('blog_thumbnail')->store('blog_thumbnail', 'public');
			    }

				if($blogs->save()){
					return response()->json(['status' => true, 'message' => 'blogs saved successfully!']);
				}
			}else{
				$filename = 'blogs_'.Str::random(8).'.txt'; 
                $ck = $this->makeCKContent($filename, $request->contents);

				$blogs = new Blog();
				$blogs->blog_title = $request->get('blog_title');
				$blogs->contents = $ck;
				$blogs->meta_description = $request->get('meta_description');
				$blogs->meta_keywords = $request->get('meta_keyword');
				
				if ($request->hasFile('blog_thumbnail') && $request->file('blog_thumbnail')->isValid()) {
			        $blogs->blog_thumbnail = 'storage/'.$request->file('blog_thumbnail')->store('blog_thumbnail', 'public');
			    }

				if($blogs->save()){
					return response()->json(['status' => true, 'message' => 'blogs saved successfully!']);
				}
			}
		}
	}

	public function edit($id){
		$title = 'Edit Blogs';
		$blogs = Blog::findOrFail($id);
		return view('Blog.add', compact('blogs', 'title'));
	}

	public function add(){
		$title = 'Add Blogs';
		return view('Blog.add', compact('title'));
	}

	public function find($id){
		$blogs = Blog::findOrFail($id);
		return response()->json(['status' => true, 'data' => $blogs ]);
	}

	public function delete($id){
		$blogs = Blog::findOrFail($id);
		if($blogs->delete()){
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}