<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Storage;

class SEOKeywordController extends Controller
{
    
    public function extractProductDetails($productUrl = '')
    {
        ini_set('max_execution_time', -1);
        
        $response = Http::withOptions([
               'verify' => "C:/wamp64/bin/php/php8.1.26/extras/ssl/cacert.pem"
              ])->get("https://www.lazada.com.ph/products/buy-1-take-1-storage-box-uniware-megabox-35li34li-i4195138724-s24296320828.html?spm=a2o4l.home-ph.3964150330.16.239eca18ybEts0&search=1&mp=1&c=fs&clickTrackInfo=rs%3A0.16530932486057281%3Bfs_item_discount_price%3A278.10%3Bitem_id%3A4195138724%3Bpctr%3A0.16530932486057281%3Bcalib_pctr%3A0.0%3Bvoucher_price%3A250.29%3Bmt%3Ahot%3Bpromo_price%3A278.1%3Bfs_utdid%3A-1%3Bfs_item_sold_cnt%3A12%3Babid%3A287818%3Bfs_item_price%3A309.00%3Bpvid%3A07746921-250b-4e1e-a05f-2c1f594cba27%3Bfs_min_price_l30d%3A0%3Bdata_type%3Aflashsale%3Bfs_pvid%3A07746921-250b-4e1e-a05f-2c1f594cba27%3Btime%3A1707965512%3Bfs_biz_type%3Afs%3Bscm%3A1007.17760.287818.%3Bchannel_id%3A0000%3Bfs_item_discount%3A10%25%3Bcampaign_id%3A268901&scm=1007.17760.287818.0");

        if ($response->successful()) {
            $htmlContent = $response->body();

            // Create a new instance of the Crawler and load the HTML content
            $crawler = new Crawler($htmlContent);

            $imageUrls = $crawler->filter('.pdp-mod-product-badge-title')->text();

            return $imageUrls;

            // // Now you can use CSS selectors to extract specific elements
            // $productName = $crawler->filter('h1.elementor-heading-title')->text();

            // // $productDescriptions = $crawler->filter('.elementor-widget-container')->first()->text();
            // $productDescription = [];
            // $foundPesoSign = false;
            // // elementor-heading-title aquaflask
           
            // //  woocommerce-product-details__short-description
            // $crawler->filter('.elementor-heading-title')->each(function (Crawler $node) use (&$productDescription, &$foundPesoSign) {
            //      if (!$foundPesoSign) { // Check if the flag is false
            //             if (strpos($node->text(), '₱') !== false) {
            //                 // Set the flag to true if "₱" sign is found
            //                 $foundPesoSign = true;
            //             } else {
            //                 // If "₱" sign is not found, concatenate the text to $productDescription
            //                 $productDescription[] = $node->text();
            //             }
            //         }
            // });

            //  $links = $crawler->filterXPath('//a[starts-with(@href, "https://")]')->extract(['href']);

            // $imageUrls = $crawler->filter('img[srcset]')->extract(['src']);
            
            // // $propertyAttributeValue = 'your_property_value';

            // // Use XPath expression to select meta tags with the specified property attribute
            // $propertyStartValue = 'og:';

            // // Use XPath expression to select meta tags with the property attribute starting with the specified value
            // $metaTags = $crawler->filterXPath("//meta[starts-with(@property, '$propertyStartValue')]");

            // $selectedMetaTags = [];

            // // Uploading
            // $imageUrl = $imageUrls[4];
            // $imageContent = file_get_contents($imageUrl);
            // $originalFilename = basename($imageUrl);
            // Storage::disk('public')->put('products/'.$originalFilename, $imageContent);
            // $storedImagePath = 'storage/products/' . $originalFilename;

            // // Iterate over the selected meta tags
            // foreach ($metaTags as $metaTag) {
            //     $property = $metaTag->getAttribute('property');

            //     if ($property == 'og:image') {
            //        $content = url('').'/'.$storedImagePath;
            //     }else{
            //        $content = str_replace('https://rs8.com.ph/', url('').'/', $metaTag->getAttribute('content'));
            //     }

            //     $selectedMetaTags[] = ["property" => $property, "content" => $content];
            // }
            // Parse the URL
            // $parsedUrl = parse_url($links[3]);

            // Reconstruct the URL without the query string
            // $newUrl = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . $parsedUrl['path'];

            // echo $newUrl;
            
            return ['metas' => $selectedMetaTags, 'name' => $productName, 'Description' => $productDescription, 'img' => url('').'/'.$storedImagePath];
        } else {
            // Handle the case where the request was not successful
            echo "Failed to retrieve content.";
        }

        // $extractKeywords = $this->extractKeywords($productUrl);
        // $extractProductName = $this->extractProductName($productUrl);
        // $productName = $this->extractProductNameOrginal($productUrl);
        // return ['keywords' => $extractKeywords, 'Description' => $extractProductName, 'productName' => $productName];
    }

    private function extractProductName($url)
    {
        // Extract the path component of the URL
        $path = parse_url($url, PHP_URL_PATH);

        // Get the last segment of the path, which should contain the product name
        $productName = Str::slug(last(explode('/', $path)));

        // Replace dashes with spaces
        $productName = str_replace('-', ' ', $productName);


         $encodedProductName = htmlspecialchars($productName, ENT_QUOTES, 'UTF-8');


        return $productName;
    }


    private function extractKeywords($url)
    {
    
        // Extract the product name from the URL
        $productName = $this->extractProductName($url);

        // Split the product name into key phrases
        $keyPhrases = $this->splitIntoKeyPhrases($productName);

        // Refine and organize the key phrases (you can add more logic here)
        $keywords = $this->refineKeywords($keyPhrases);

        // Return the extracted keywords
        return response()->json(['keywords' => $keywords]);
    }

    private function extractProductNameOrginal($url){
        // Get the URL from the request
        // $url = $request->url();
        
        // Parse the URL
        $parsedUrl = parse_url($url);
        
        // Get the path from the parsed URL
        $path = $parsedUrl['path'];
        
        // Split the path by "/"
        $pathParts = explode("/", $path);
        
        // The product name is usually the last part of the path
        $productName = end($pathParts);
        
        // Output or use the product name as needed
        return $productName;
    }

    private function splitIntoKeyPhrases($productName)
    {
        // Split the product name into key phrases based on spaces
        return explode(' ', $productName);
    }

    private function refineKeywords($keyPhrases)
    {
        // Here you can add more logic to refine and organize the keywords
        // For example, you could remove stop words, filter out irrelevant terms, or combine related phrases

        return implode(', ', $keyPhrases);
    }
}