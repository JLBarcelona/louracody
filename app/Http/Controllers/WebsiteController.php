<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Support\Facades\Storage;
use File;

use Validator;
use Str;
use Illuminate\Support\Facades\Http;


class WebsiteController extends Controller
{

	public function upload(Request $request){

      if ($request->hasFile('upload')) {

          $file = $request->file('upload'); 
          $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

          $allowed = array('jpg','jpeg','png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'); // allowed File extensions

            $extension = $file->extension();
            if(!in_array($extension, $allowed)){
                return "Please select valid image file.";
            }

          $newFileName  = preg_replace('/\s+/', '_', $fileName);
          $fileName = $newFileName . '_' . time() . '.' . $file->getClientOriginalExtension();
          $file->move(public_path('ckeditor_uploads/'), $fileName); 
      
          $ckeditor = $request->input('CKEditorFuncNum');
          $url = asset('ckeditor_uploads/' . str_replace(' ', '_', $fileName));
          $msg = 'Image uploaded successfully';

          $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";
          return response()->json(['fileName' => $fileName, 'uploaded' => 1, 'url' => $url, 'data' => $response]);

      }

    }


	public function terms(){
		return view('site.terms');
	}

	public function privacy(){
		return view('site.privacy');
	}

	public function generator(){
		return view('site.generator');
	}
	
	public function blogs(){
		$blogs = Blog::all();
		return view('site.blogs', compact('blogs'));
	}

	public function blogsDetails($slug){
		$blog = Blog::where('blog_title', $slug)->firstOrFail();
		return view('site.blog_details', compact('blog'));
	}

	public function aboutUs(){
		return view('site.about_us');
	}

	public function productDetails(Request $request, $slug){
		$link_extend = '';
		$all = $request->all();
		
		$i = 0;
		foreach ($all as $key => $value) {
			if ($i == 0) {
				$link_extend .= '?'.$key.'='.$value;
			}else{
				$link_extend .= '&'.$key.'='.$value;
			}
			$i++;
		}

	    $product = Product::where('slug', $slug)->firstOrFail();
	    $metas = json_decode($product->meta_code);
	    // return $data;

	    return view('site.product_details', compact('metas', 'product', 'link_extend'));

	}


	public function productLink(Request $request, $slug){
		$link_extend = '';
		$all = $request->all();
		
		$i = 0;
		foreach ($all as $key => $value) {
			if ($i == 0) {
				$link_extend .= '&'.$key.'='.$value;
			}else{
				$link_extend .= '&'.$key.'='.$value;
			}
			$i++;
		}


		$product = Product::where('slug', $slug)->firstOrFail();

		$res = Http::withoutRedirecting()->get($product->involve_link_shopee);
		$urlFinal = $res->header('Location');

		$res2 = Http::withoutRedirecting()->get($urlFinal);
		$urlFinal2 = $res2->header('Location');

		$fUrl = $urlFinal2.$link_extend;

		ProductView::create(['url' => route('product_link', $slug), 'ip_address' => request()->ip(), 'user_agent' => request()->header('User-Agent'), 'product_id' => $product->id]);
		return redirect($fUrl);
	}

	public function index(Request $request){
		$link_extend = '';
		$all = $request->all();
		
		$i = 0;
		foreach ($all as $key => $value) {
			if ($i == 0) {
				$link_extend .= '?'.$key.'='.$value;
			}else{
				$link_extend .= '&'.$key.'='.$value;
			}
			$i++;
		}

		return view('site.index', compact('link_extend'));
	}

	public function loadData(Request $request) {
		$keyword = $request->get('keyword');
	    $data = Product::orderBy('id', 'desc');

		if (!empty($keyword)) {
			$data = $data->where('search_keyword', 'LIKE', '%'.$keyword.'%')->orWhere('name', 'LIKE', '%'.$keyword.'%');
		}

		$data = $data->paginate(8);

	    return response()->json($data);
	}

	public function shops(){
		return view('site.shops');
	}

}