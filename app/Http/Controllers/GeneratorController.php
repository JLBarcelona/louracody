<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class GeneratorController extends Controller
{

	public function generate(Request $request){
		$inputs = $request->get('gen');
		$controller_name = $request->get('controller');
		$model_name = $request->get('model');
		$prefix = strtolower($request->get('prefix'));
		$primary_key = $request->get('primary_key');
		$form_id = $request->get('form_id');
		$form_type = $request->get('form_type');
		$bootstrap_version = $request->get('bootstrap_version');

		$data = [];

		$view = '';
		$view_form = '';
		$table = '';
		$form = '';
		$javascript = '';
		$js_field = '';
		$js_field_data = '';
		$route  = '';
		$controller = '';
		$model = '';
		$controller_container = '';
		$text_right = ($bootstrap_version == 4)? 'text-right' : 'text-end';
		$controller_validation = '';
		$controller_data = '';
		$model_values = '';
		$js_list = '';
		$javascript_form = '';
		$tab_mode = ($form_type == 'modal')? 6 : 5;

		$has_edit_id = ($form_type == 'modal')? '' : 'value="{{ $'.$prefix.'->'.$primary_key.' ?? \'\' }}"';
		$form .= $this->space(1,($tab_mode)).'<input type="hidden" name="'.$primary_key.'" id="'.$primary_key.'" '.$has_edit_id.'/>';
		foreach ($inputs as  $value) {
			$type = $value['type'];
			$id_name = $value['id_name'];
			$class = $value['class'];
			$placeholder = $value['placeholder'];
			$grid = $value['grid'];
			$required = (!empty($value['is_requried']) && $value['is_requried'] == 1)? 'required' : '';
			$email_validator = ($type == 'email')? '|email' : '';
			$password_validator = ($type == 'password')? '|min:8' : '';
			$has_edit = ($form_type == 'modal')? '' : 'value="{{ $'.$prefix.'->'.$id_name.' ?? \'\' }}"';
			$has_edit_textarea = ($form_type == 'modal')? '' : '{{ $'.$prefix.'->'.$id_name.' ?? \'\' }}';

			$form .= $this->space(1,$tab_mode).'<div class="'.$grid.' mb-3">';
				$form .= $this->space(1,($tab_mode + 1)).'<label class="mb-2">'.ucfirst(str_replace('_', ' ', $id_name)).'</label>';
				if ($type == 'select') {
					$form .= $this->space(1,($tab_mode + 1)).'<select name="'.$id_name.'" id="'.$id_name.'" class="form-control '.$class.'" '.$required.'>';
					$form .= $this->space(1,($tab_mode + 1)).'</select>';
				}elseif ($type == 'textarea') {
					$form .= $this->space(1,($tab_mode + 1)).'<textarea type="'.$type.'" name="'.$id_name.'" id="'.$id_name.'" class="form-control '.$class.'" '.$required.' placeholder="'.$placeholder.'">'.$has_edit_textarea.'</textarea>';
				}else{
					$form .= $this->space(1,($tab_mode + 1)).'<input type="'.$type.'" name="'.$id_name.'" id="'.$id_name.'" class="form-control '.$class.'" '.$required.' '.$has_edit.' placeholder="'.$placeholder.'"/>';
				}
			$form .= $this->space(1,$tab_mode).'</div>';


			if (!empty($required)) {
				$controller_validation .= $this->space(1,3).'\''.$id_name.'\' => \'required'.$email_validator.$password_validator.'\',';
			}

			$js_field .= $this->space(1,1).'$(\'#'.$id_name.'\').val(\'\');';

			$js_field_data .= $this->space(1,4).'$(\'#'.$id_name.'\').val(response.data.'.$id_name.');';

			$model_values .= $this->space(1, 2).'\''.$id_name.'\',';

			$controller_data .= $this->space(1,5).'\''.$id_name.'\' => $request->get(\''.$id_name.'\'),';


			$js_list .= $this->space(1,3).'{';
			$js_list .= $this->space(1,4).'className: \'\',';
			$js_list .= $this->space(1,4).'"data": \''.$id_name.'\',';
			$js_list .= $this->space(1,4).'"title": \''.ucfirst(str_replace('_', ' ', $id_name)).'\',';
			$js_list .= $this->space(1,3).'},';

		}

		// Form
		

		
		$id_edit = ($form_type == 'modal')? '' : '/{{ $'.$prefix.'->'.$primary_key.' ?? \'\' }}';


		
		$form_tab = ($form_type !== 'modal')? 3 : 0; 


		$view .= $this->space(1,$form_tab).'<form method="post" class="needs-validation" id="'.$form_id.'" action="{{ route(\''.$prefix.'.save\') }}'.$id_edit.'" novalidate>';
			if ($form_type == 'modal') {
					if ($bootstrap_version == 4) {
						$view .= $this->space(1,1).'<div class="modal fade" tabindex="-1" role="dialog" id="modal_'.$form_id.'" data-backdrop="static">';
					}else{
						$view .= $this->space(1,1).'<div class="modal fade" tabindex="-1" role="dialog" id="modal_'.$form_id.'" data-bs-backdrop="static">';
					}
					$view .= $this->space(1,2).'<div class="modal-dialog" role="document">';
						$view .= $this->space(1,3).'<div class="modal-content">';
							$view .= $this->space(1,4).'<div class="modal-header">';
								$view .= $this->space(1,5).'<h5 class="modal-title" id="modal_'.$form_id.'_title">Add '.ucfirst(str_replace('_', ' ', $prefix)).'</h5>';
								if ($bootstrap_version == 4) {
									$view .= $this->space(1,5).'<button class="close" data-dismiss="modal">&times;</button>';
								}else{
									$view .= $this->space(1,5).'<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" aria-hidden="true"></button>';
								}
							$view .= $this->space(1,4).'</div>';
							$view .= $this->space(1,4).'<div class="modal-body">';
								$view .= $this->space(1,5).'<div class="row">';
									$view .= $form;
								$view .= $this->space(1,5).'</div>';
							$view .= $this->space(1,4).'</div>';
							$view .= $this->space(1,4).'<div class="modal-footer">';
								if ($bootstrap_version == 4) {
									$view .= $this->space(1,5).'<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>';
								}else{
									$view .= $this->space(1,5).'<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>';
								}
								$view .= $this->space(1,5).'<button type="submit" class="btn btn-success" id="'.$form_id.'_btn" >Save</button>';
							$view .= $this->space(1,4).'</div>';
						$view .= $this->space(1,3).'</div>';
					$view .= $this->space(1,2).'</div>';
				$view .= $this->space(1,1).'</div>';
			}else{
				$view .= $this->space(1,4).'<div class="row">';
					$view .= $form;
				$view .= $this->space(1,5).'<div class="col-md-12 '.$text_right.'">';
				$view .= $this->space(1,6).'<button type="submit" class="btn btn-success" id="'.$form_id.'_btn" >Save</button>';
				$view .= $this->space(1,5).'</div>';	
				$view .= $this->space(1,4).'</div>';
			}
		$view .= $this->space(1,$form_tab).'</form>';



		if ($form_type !== 'modal') {
			$view_form .= $this->space().'<div class="container-fluid">';
			$view_form .= $this->space(1,1).'<div class="row">';
			$view_form .= $this->space(1,2).'<div class="col-md-12"><h1>{{ $title }}</h2></div>';
			$view_form .= $this->space(1,2).'<div class="col-md-12">';
			$view_form .= $view;
			$view_form .= $this->space(1,2).'</div>';

			$view_form .= $this->space(1,1).'</div>';
			$view_form .= $this->space(1).'</div>'.$this->space(2);
		}


		$table .= $this->space().'<div class="container-fluid pt-3">';
		$table .= $this->space(1,1).'<div class="row mb-3">';

		if ($form_type !== 'modal') {
			$table .= $this->space(1,2).'<div class="col-md-6 text-capitalize"><h1>'.ucfirst(str_replace('_', ' ', $prefix)).' Management</h2></div>';
			$table .= $this->space(1,2).'<div class="col-md-6 '.$text_right.'"><a href="{{ route(\''.$prefix.'.add\') }}" class="btn btn-primary">Add '.ucfirst(str_replace('_', ' ', $prefix)).'</a></div>';
		}else{
			$table .= $this->space(1,2).'<div class="col-md-6 text-capitalize"><h1>'.ucfirst(str_replace('_', ' ', $prefix)).' Management</h2></div>';
			$table .= $this->space(1,2).'<div class="col-md-6 '.$text_right.'"><button type="button" onclick="add_'.$prefix.'();" class="btn btn-primary">Add '.ucfirst(str_replace('_', ' ', $prefix)).'</button></div>';
		}


		$table .= $this->space(1,2).'<div class="col-md-12">';
			$table .= $this->space(1,3).'<table class="table table-bordered table-striped" id="tbl_'.$prefix.'" style="width: 100%;"></table>';
			$table .= $this->space(1,2).'</div>';			
			$table .= $this->space(1,1).'</div>';
		$table .= $this->space(1).'</div>';

		if ($form_type == 'modal') {
			$table .= $this->space(2).'<!-- Modal Form -->';				
			$table .= $view;				
		}



		// Controller
		// index
		$controller .= $this->space(1,1).'public function index(){';
		$controller .= $this->space(1,2).'return view(\''.$model_name.'.index\');';
		$controller .= $this->space(1,1).'}';
		$controller .= $this->space(1);

		// list
		$controller .= $this->space(1,1).'public function list(){';
			$controller .= $this->space(1,2).'$'.$prefix.' = '.$model_name.'::get();';
			$controller .= $this->space(1,2).'return response()->json([\'status\' => true, \'data\' => $'.$prefix.' ]);';
		$controller .= $this->space(1,1).'}';
		$controller .= $this->space(1);

	
		// save 
		$controller .= $this->space(1,1).'public function save(Request $request, $'.$primary_key.' = ""){';
			$controller .= $this->space(1,2).'$validator = Validator::make($request->all(), [';
			$controller .= $controller_validation;
			$controller .= $this->space(1,2).']);';
			$controller .= $this->space(1);
			$controller .= $this->space(1,2).'if($validator->fails()){';
				$controller .= $this->space(1,3).'return response()->json([\'status\' => false, \'error\' => $validator->errors() ]);';
			$controller .= $this->space(1,2).'}else{';
				$controller .= $this->space(1,3).'if(!empty($'.$primary_key.')){';
				$controller .= $this->space(1,4).'$'.$prefix.' = '.$model_name.'::where(\''.$primary_key.'\', $'.$primary_key.')->update([';
							$controller .= $controller_data;
				$controller .= $this->space(1,4).']);';
				$controller .= $this->space(1,4).'if($'.$prefix.'){';
				$controller .= $this->space(1,5).'return response()->json([\'status\' => true, \'message\' => \''.str_replace('_', ' ', $prefix).' saved successfully!\']);';
				$controller .= $this->space(1,4).'}';
				$controller .= $this->space(1,3).'}else{';
				$controller .= $this->space(1,4).'$'.$prefix.' = '.$model_name.'::create([';
							$controller .= $controller_data;
				$controller .= $this->space(1,4).']);';
				$controller .= $this->space(1,4).'if($'.$prefix.'){';
				$controller .= $this->space(1,5).'return response()->json([\'status\' => true, \'message\' => \''.str_replace('_', ' ', $prefix).' updated successfully!\']);';
				$controller .= $this->space(1,4).'}';
				$controller .= $this->space(1,3).'}';
			$controller .= $this->space(1,2).'}';

		$controller .= $this->space(1,1).'}';
		$controller .= $this->space(1);


		if ($form_type !== 'modal') {
			// edit
			$controller .= $this->space(1,1).'public function edit($'.$primary_key.'){';
			$controller .= $this->space(1,2).'$title = \'Edit '.ucfirst(str_replace('_', ' ', $prefix)).'\';';
			$controller .= $this->space(1,2).'$'.$prefix.' = '.$model_name.'::findOrFail($'.$primary_key.');';
			$controller .= $this->space(1,2).'return view(\''.$model_name.'.add\', compact(\''.$prefix.'\', \'title\'));';
			$controller .= $this->space(1,1).'}';
			$controller .= $this->space(1);

			// add
			$controller .= $this->space(1,1).'public function add(){';
			$controller .= $this->space(1,2).'$title = \'Add '.ucfirst(str_replace('_', ' ', $prefix)).'\';';
			$controller .= $this->space(1,2).'return view(\''.$model_name.'.add\', compact(\'title\'));';
			$controller .= $this->space(1,1).'}';
			$controller .= $this->space(1);
		}
		
		// find
		$controller .= $this->space(1,1).'public function find($'.$primary_key.'){';
			$controller .= $this->space(1,2).'$'.$prefix.' = '.$model_name.'::findOrFail($'.$primary_key.');';
			$controller .= $this->space(1,2).'return response()->json([\'status\' => true, \'data\' => $'.$prefix.' ]);';
		$controller .= $this->space(1,1).'}';
		$controller .= $this->space(1);

		// delete
		$controller .= $this->space(1,1).'public function delete($'.$primary_key.'){';
			$controller .= $this->space(1,2).'$'.$prefix.' = '.$model_name.'::findOrFail($'.$primary_key.');';
			$controller .= $this->space(1,2).'if($'.$prefix.'->delete()){';
				$controller .= $this->space(1,3).'return response()->json([\'status\' => true, \'message\' => \'Record deleted successfully!\' ]);';
			$controller .= $this->space(1,2).'}';
		$controller .= $this->space(1,1).'}';
		$controller .= $this->space(1);

		$controller_container .= $this->space().'<?php';
		$controller_container .= $this->space(1).'namespace App\Http\Controllers;';
		$controller_container .= $this->space(1).'use Illuminate\Http\Request;';
		$controller_container .= $this->space(1).'use App\Models\\'.$model_name.';';
		$controller_container .= $this->space(1).'use Validator;';
		$controller_container .= $this->space(2).'class '.$controller_name.' extends Controller';
		$controller_container .= $this->space(1).'{';
		$controller_container .= $controller;
		$controller_container .= $this->space(1).'}';

		// model

		$model .= $this->space().'<?php';
		$model .= $this->space(1).'namespace App\Models;';
		$model .= $this->space(1).'use Illuminate\Database\Eloquent\Factories\HasFactory;';
		$model .= $this->space(1).'use Illuminate\Database\Eloquent\Model;';
		$model .= $this->space(2).'class '.$model_name.' extends Model';
		$model .= $this->space(1).'{';
		$model .= $this->space(1,1).'use HasFactory;';
		$model .= $this->space(2,1).'protected $fillable = [';
		$model .= $model_values;
		$model .= $this->space(1,1).'];';

		$model .= $this->space(1).'}';


		// Javascript
			
			// list
			$javascript .= $this->space().'var tbl_'.$prefix.';';
			$javascript .= $this->space(1).'show_'.$prefix.'();';
			$javascript .= $this->space(1).'function show_'.$prefix.'(){';
			$javascript .= $this->space(1,1).'if(tbl_'.$prefix.'){';
			$javascript .= $this->space(1,2).'tbl_'.$prefix.'.destroy();';
			$javascript .= $this->space(1,1).'}';
			$javascript .= $this->space(1,1).'tbl_samples = $(\'#tbl_'.$prefix.'\').DataTable({';
			$javascript .= $this->space(1,2).'destroy: true,';
			$javascript .= $this->space(1,2).'pageLength: 10,';
			$javascript .= $this->space(1,2).'responsive: true,';
			$javascript .= $this->space(1,2).'ajax: "{{ route(\''.$prefix.'.list\') }}",';
			$javascript .= $this->space(1,2).'deferRender: true,';
			$javascript .= $this->space(1,2).'columns: [';
			$javascript .= $js_list;
			$javascript .= $this->space(1,3).'{';
			$javascript .= $this->space(1,4).'className: \'width-option-1 text-center\',';
			$javascript .= $this->space(1,4).'width: \'15%\',';
			$javascript .= $this->space(1,4).'"data": \''.$primary_key.'\',';
			$javascript .= $this->space(1,4).'"orderable": false,';
			$javascript .= $this->space(1,4).'"title": \'Options\',';
			$javascript .= $this->space(1,4).'"render": function(data, type, row, meta){';
			$javascript .= $this->space(1,5).'newdata = \'\';';
			if ($form_type == 'modal') {
				$javascript .= $this->space(1,5).'newdata += \'<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_'.$prefix.'(\'+row.'.$primary_key.'+\')" type="button"><i class="fa fa-edit"></i></button> \';';
			}else{
				$javascript .= $this->space(1,5).'newdata += \'<a href="{{ route(\''.$prefix.'.edit\') }}/\'+row.'.$primary_key.'+\'" class="btn btn-success btn-sm font-base mt-1" ><i class="fa fa-edit"></i></a> \';';
			}


			$javascript .= $this->space(1,5).'newdata += \' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_'.$prefix.'(\'+row.'.$primary_key.'+\');" type="button"><i class="fa fa-trash"></i></button>\';';
			$javascript .= $this->space(1,5).'return newdata;';
			$javascript .= $this->space(1,4).'}';
			$javascript .= $this->space(1,3).'}';
			$javascript .= $this->space(1,2).']';
			$javascript .= $this->space(1,1).'});';
			$javascript .= $this->space(1).'}'.$this->space(1);

			// Form submit
			if ($form_type == 'modal') {
				$javascript .= $this->space(2).'$("#'.$form_id.'").on(\'submit\', function(e){';
				$javascript .= $this->space(1,1).'e.preventDefault();';
				$javascript .= $this->space(1,1).'let id = $(\'#'.$primary_key.'\').val();';
				$javascript .= $this->space(1,1).'let url = $(this).attr(\'action\');';
				$javascript .= $this->space(1,1).'let formData = $(this).serialize();';
				$javascript .= $this->space(1,1).'$.ajax({';
					$javascript .= $this->space(1,2).'type:"POST",';
					$javascript .= $this->space(1,2).'url:url+\'/\'+id,';
					$javascript .= $this->space(1,2).'data:formData,';
					$javascript .= $this->space(1,2).'dataType:\'json\',';
					$javascript .= $this->space(1,2).'beforeSend:function(){';
					$javascript .= $this->space(1,3).'$(\'#'.$form_id.'_btn\').prop(\'disabled\', true);';
					$javascript .= $this->space(1,2).'},';
					$javascript .= $this->space(1,2).'success:function(response){';
						$javascript .= $this->space(1,3).'// console.log(response);';
						$javascript .= $this->space(1,3).'if (response.status == true) {';
								$javascript .= $this->space(1,4).'swal("Success", response.message, "success");';
								if ($form_type == 'modal') {
									$javascript .= $this->space(1,4).'show_'.$prefix.'();';
									$javascript .= $this->space(1,4).'$(\'#modal_'.$form_id.'\').modal(\'hide\');';
								}else{
									$javascript .= $this->space(1,4).'setTimeout(function(){';
									$javascript .= $this->space(1,5).'window.location = "{{ route(\''.$prefix.'.index\') }}";';
									$javascript .= $this->space(1,4).'}, 1500);';

								}
						$javascript .= $this->space(1,3).'}else{';
							$javascript .= $this->space(1,4).'console.log(response);';
						$javascript .= $this->space(1,3).'}';
						$javascript .= $this->space(1,4).'validation(\''.$form_id.'\', response.error);';
						$javascript .= $this->space(1,4).'$(\'#'.$form_id.'_btn\').prop(\'disabled\', false);';
					$javascript .= $this->space(1,2).'},';
						$javascript .= $this->space(1,2).'error: function(error){';
							$javascript .= $this->space(1,3).'$(\'#'.$form_id.'_btn\').prop(\'disabled\', false);';
							$javascript .= $this->space(1,3).'console.log(error);';
					$javascript .= $this->space(1,2).'}';
				$javascript .= $this->space(1,1).'});';
			$javascript .= $this->space(1).'});';
			}else{
				$javascript_form .= $this->space(2).'$("#'.$form_id.'").on(\'submit\', function(e){';
				$javascript_form .= $this->space(1,1).'e.preventDefault();';
				$javascript_form .= $this->space(1,1).'let url = $(this).attr(\'action\');';
				$javascript_form .= $this->space(1,1).'let formData = $(this).serialize();';
				$javascript_form .= $this->space(1,1).'$.ajax({';
					$javascript_form .= $this->space(1,2).'type:"POST",';
					$javascript_form .= $this->space(1,2).'url:url,';
					$javascript_form .= $this->space(1,2).'data:formData,';
					$javascript_form .= $this->space(1,2).'dataType:\'json\',';
					$javascript_form .= $this->space(1,2).'beforeSend:function(){';
					$javascript_form .= $this->space(1,3).'$(\'#'.$form_id.'_btn\').prop(\'disabled\', true);';
					$javascript_form .= $this->space(1,2).'},';
					$javascript_form .= $this->space(1,2).'success:function(response){';
						$javascript_form .= $this->space(1,3).'// console.log(response);';
						$javascript_form .= $this->space(1,3).'if (response.status == true) {';
								$javascript_form .= $this->space(1,4).'swal("Success", response.message, "success");';
								if ($form_type == 'modal') {
									$javascript_form .= $this->space(1,4).'$(\'#modal_'.$form_id.'\').modal(\'hide\');';
								}else{
									$javascript_form .= $this->space(1,4).'setTimeout(function(){';
									$javascript_form .= $this->space(1,5).'window.location = "{{ route(\''.$prefix.'.index\') }}";';
									$javascript_form .= $this->space(1,4).'}, 1500);';

								}
						$javascript_form .= $this->space(1,3).'}else{';
							$javascript_form .= $this->space(1,4).'console.log(response);';
						$javascript_form .= $this->space(1,3).'}';
						$javascript_form .= $this->space(1,4).'validation(\''.$form_id.'\', response.error);';
						$javascript_form .= $this->space(1,4).'$(\'#'.$form_id.'_btn\').prop(\'disabled\', false);';
					$javascript_form .= $this->space(1,2).'},';
						$javascript_form .= $this->space(1,2).'error: function(error){';
							$javascript_form .= $this->space(1,3).'$(\'#'.$form_id.'_btn\').prop(\'disabled\', false);';
							$javascript_form .= $this->space(1,3).'console.log(error);';
					$javascript_form .= $this->space(1,2).'}';
				$javascript_form .= $this->space(1,1).'});';
			$javascript_form .= $this->space(1).'});';
			}


			if ($form_type == 'modal') {
				// Add
				$javascript .= $this->space(2).'function add_'.$prefix.'(){';
				$javascript .= $this->space(1, 1).'$("#'.$primary_key.'").val(\'\');';
				$javascript .= $js_field;
				$javascript .= $this->space(1, 1).'$("#modal_'.$form_id.'").modal(\'show\');';
				$javascript .= $this->space(1).'}'.$this->space(1);


				// Edit
				$javascript .= $this->space(2).'function edit_'.$prefix.'('.$primary_key.'){';
				$javascript .= $this->space(1,1).'$.ajax({';
				$javascript .= $this->space(1,2).'type:"GET",';
				$javascript .= $this->space(1,2).'url:"{{ route(\''.$prefix.'.find\') }}/"+'.$primary_key.',';
				$javascript .= $this->space(1,2).'data:{},';
				$javascript .= $this->space(1,2).'dataType:\'json\',';
				$javascript .= $this->space(1,2).'beforeSend:function(){';
				$javascript .= $this->space(1,2).'},';
				$javascript .= $this->space(1,2).'success:function(response){';
				$javascript .= $this->space(1,3).'// console.log(response);';
				$javascript .= $this->space(1,3).'if (response.status == true) {';
				$javascript .= $this->space(1,4).'$(\'#'.$primary_key.'\').val(response.data.'.$primary_key.');';
				$javascript .= $js_field_data;
				$javascript .= $this->space(1,4).'$(\'#modal_'.$form_id.'\').modal(\'show\');';
				$javascript .= $this->space(1,3).'}else{';
				$javascript .= $this->space(1,4).'console.log(response);';
				$javascript .= $this->space(1,3).'}';
				$javascript .= $this->space(1,2).'},';
				$javascript .= $this->space(1,2).'error: function(error){';
				$javascript .= $this->space(1,3).'console.log(error);';
				$javascript .= $this->space(1,2).'}';
				$javascript .= $this->space(1,1).'});';
				$javascript .= $this->space(1).'}'.$this->space(1);
			}

			


			$javascript .= $this->space(2).'function delete_'.$prefix.'('.$primary_key.'){';
			$javascript .= $this->space(1,1).'swal({';
			$javascript .= $this->space(1,2).'title: "Are you sure?",';
			$javascript .= $this->space(1,2).'text: "Do you want to delete '.strtolower(str_replace('_', ' ', $prefix)).'?",';
			$javascript .= $this->space(1,2).'type: "warning",';
			$javascript .= $this->space(1,2).'showCancelButton: true,';
			$javascript .= $this->space(1,2).'confirmButtonColor: "#DD6B55",';
			$javascript .= $this->space(1,2).'confirmButtonText: "Yes",';
			$javascript .= $this->space(1,2).'closeOnConfirm: false';
			$javascript .= $this->space(1,1).'},';
			$javascript .= $this->space(1,1).'function(){';
			$javascript .= $this->space(1,2).'$.ajax({';
			$javascript .= $this->space(1,3).'type:"DELETE",';
			$javascript .= $this->space(1,3).'url:"{{ route(\''.$prefix.'.delete\') }}/"+'.$primary_key.',';
			$javascript .= $this->space(1,3).'data:{},';
			$javascript .= $this->space(1,3).'dataType:\'json\',';
			$javascript .= $this->space(1,3).'beforeSend:function(){';
			$javascript .= $this->space(1,2).'},';
			$javascript .= $this->space(1,2).'success:function(response){';
			$javascript .= $this->space(1,3).'// console.log(response);';
			$javascript .= $this->space(1,3).'if (response.status == true) {';
			$javascript .= $this->space(1,4).'show_'.$prefix.'();';
			$javascript .= $this->space(1,4).'swal("Success", response.message, "success");';
			$javascript .= $this->space(1,3).'}else{';
			$javascript .= $this->space(1,4).'console.log(response);';
			$javascript .= $this->space(1,3).'}';
			$javascript .= $this->space(1,2).'},';
			$javascript .= $this->space(1,2).'error: function(error){';
			$javascript .= $this->space(1,3).'console.log(error);';
			$javascript .= $this->space(1,2).'}';
			$javascript .= $this->space(1,2).'});';
			$javascript .= $this->space(1,1).'});';
			$javascript .= $this->space(1).'}'.$this->space(1);



		// route 
		$route  .= $this->space().'use App\\Http\\Controllers\\'.$controller_name.';';
		$route  .= $this->space(1).'/* Put it at the top */';



		$route  .= $this->space(2).'Route::group([\'prefix\' => \''.str_replace('_', '-', $prefix).'\', \'as\' => \''.$prefix.'.\'], function(){';
			$route  .= $this->space(1,1).'Route::controller('.$controller_name.'::class)->group(function () {';
				$route  .= $this->space(1,2).'Route::get(\'\', \'index\')->name(\'index\');';
				$route  .= $this->space(1,2).'Route::get(\'list\', \'list\')->name(\'list\');';
				if ($form_type !== 'modal') {
					$route  .= $this->space(1,2).'Route::get(\'add\', \'add\')->name(\'add\');';
					$route  .= $this->space(1,2).'Route::get(\'edit/{'.$primary_key.'?}\', \'edit\')->name(\'edit\');';
				}
				$route  .= $this->space(1,2).'Route::post(\'save/{'.$primary_key.'?}\', \'save\')->name(\'save\');';
				$route  .= $this->space(1,2).'Route::get(\'find/{'.$primary_key.'?}\', \'find\')->name(\'find\');';
				$route  .= $this->space(1,2).'Route::delete(\'delete/{'.$primary_key.'?}\', \'delete\')->name(\'delete\');';
			$route  .= $this->space(1,1).'});';
		$route  .= $this->space(1).'});';

		return response()->json(['status' => true, 'view' => $view_form, 'route' => $route, 'controller' => $controller_container, 'javascript' => $javascript, 'model' => $model, 'table' => $table, 'form_type' => $form_type, 'javascript_form' => $javascript_form]);
	}

	public function space($line = 0, $tab = 0){
		$lines = '';
		for ($i=0; $i < $line ; $i++) { 
			$lines .="\n";
		}

		$tabs = '';
			for ($ii=0; $ii < $tab ; $ii++) { 
				$tabs .="\t";
			}

		return $lines.$tabs;
	}
}
