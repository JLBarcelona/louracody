<?php 

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Storage;

class ContentCrawlService
{
    public function generate($url){
		  $stage = env('APP_ENV');
		  $response = [];

		  if ($stage == 'local') {
		  	$response = Http::withOptions(['verify' => "C:/wamp64/bin/php/php8.1.26/extras/ssl/cacert.pem"])->get($url);
		  }else{
		  	$response = Http::get($url);
		  }
    	  

        if ($response->successful()) {
            $htmlContent = $response->body();
            $crawler = new Crawler($htmlContent);
            $productName = $crawler->filter('h1.elementor-heading-title')->text();
            $productDescription = [];
            $foundPesoSign = false;
            $crawler->filter('.elementor-widget-container:first-child > p')->each(function (Crawler $node) use (&$productDescription, &$foundPesoSign) {
             if (!$foundPesoSign) {
                    if (strpos($node->text(), '₱') !== false) {
                        $foundPesoSign = true;
                    } else {
                        $productDescription[] = $node->text();
                    }
                }
            });
            $links = $crawler->filterXPath('//a[starts-with(@href, "https://shopee")]')->extract(['href']);
            $imageUrls = $crawler->filter('img[srcset]')->extract(['src']);	            
            $propertyStartValue = 'og:';
            $metaTags = $crawler->filterXPath("//meta[starts-with(@property, '$propertyStartValue')]");
            $selectedMetaTags = [];
            $imageUrl = $imageUrls[6];
            $imageContent = file_get_contents($imageUrl);
            $originalFilename = basename($imageUrl);
            Storage::disk('public')->put('products/'.$originalFilename, $imageContent);
            $storedImagePath = 'storage/products/' . $originalFilename;
            foreach ($metaTags as $metaTag) {
                $property = $metaTag->getAttribute('property');

                if ($property == 'og:image') {
                   $content = url('').'/'.$storedImagePath;
                }else{
                   $content = str_replace('https://rs8.com.ph/', url('').'/', $metaTag->getAttribute('content'));
                }

                $selectedMetaTags[] = ["property" => $property, "content" => $content];
            }
            $parsedUrl = parse_url($links[3]);
            $newUrl = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . $parsedUrl['path'];            
         
			$compress_extract = explode(' ', $productDescription[0]);


            return ['status' => true,
            		 'slug' => str_replace([url('/product'), '/'], '', $selectedMetaTags[4]['content']),
            		 'product_name' => $productName,
            		 'description' => implode("\n", $productDescription),
            		 'shopee_url' => $newUrl,
            		 'search_keyword' => strtolower(str_replace(',,', ',', implode(',', $compress_extract))),
            		 'meta_codes' => json_encode($selectedMetaTags),
            		 'img' => $storedImagePath];

        } else {
           return ['status' => false];
        }
    }
}