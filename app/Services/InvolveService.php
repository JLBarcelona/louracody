<?php 

namespace App\Services;

use Illuminate\Support\Facades\Http;
use App\Models\Product;
use Str;

class InvolveService
{
    private function auth(){
		$response = Http::post('https://api.involve.asia/api/authenticate', [
		    'secret' => env('INVOLVE_SECRET'),
		    'key' => env('INVOLVE_KEY'),
		]);

		return $response->json()['data']['token'];
    }

    private function maxProductId(){
    	$max = Product::max('id');
		return Str::random(3).$max.Str::random(5);
    }

    private function convertLinkShopee($or_link){
    	$url = env('SHOPEE_URL'); 
		$afid = env('SHOPEE_ID'); 
		$orlink = urlencode($or_link);
		$sub = $this->maxProductId();

		$replace_orgin  = str_replace(['orlink', 'afid', 'subid'], [$orlink, $afid, $sub], $url);

		return $replace_orgin;
    }


    private function ShopeeOffer(){
    	$key = $this->auth();

    	$response = Http::withHeaders([
		    'Authorization' => 'Bearer '.$key,
		])->post('https://api.involve.asia/api/offers/all', [
		    'page' => 1,
		    'limit' => 100,
		    'sort_by' => 'relevance',
		    'filters' => [
		        'offer_name' => 'Shopee',
		    ],
		]);

		return $response->json()['data']['data'][0]['offer_id'];
    }

    public function generate($url){

    	$url_convert = $this->convertLinkShopee($url);
    	
    	$key = $this->auth();
    	$offer_id = $this->ShopeeOffer();
    	// 4244
    	$response = Http::withHeaders([
		    'Authorization' => 'Bearer '.$key,
		])->post('https://api.involve.asia/api/deeplink/generate', [
		    'offer_id' => intval($offer_id),
		    'url' => $url_convert
		]);

		return $response->json()['data']['tracking_link'];
    }
}