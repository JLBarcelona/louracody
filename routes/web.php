<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\SEOKeywordController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\GeneratorController;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes();
// login routes


Route::get('', [App\Http\Controllers\WebsiteController::class, 'index'])->name('website');
Route::get('about-us', [App\Http\Controllers\WebsiteController::class, 'aboutUs'])->name('about_us');
Route::get('blog-post', [App\Http\Controllers\WebsiteController::class, 'blogs'])->name('blogs');
Route::get('blog-post/{slug}', [App\Http\Controllers\WebsiteController::class, 'blogsDetails'])->name('blogs_details');
Route::get('generator', [App\Http\Controllers\WebsiteController::class, 'generator'])->name('generator');
Route::get('privacy-policy', [App\Http\Controllers\WebsiteController::class, 'privacy'])->name('privacy');
Route::get('terms-and-condition', [App\Http\Controllers\WebsiteController::class, 'terms'])->name('terms');
Route::get('/ads.txt', function () {
    $content = view('ads');
    return response($content, 200)
        ->header('content-Type', 'text');
});

Route::post('generate', [GeneratorController::class, 'generate'])->name('generate');

// dashboard
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Route::group(['prefix' => 'password', 'as' => 'password.'], function(){
	Route::controller(ForgotPasswordController::class)->group(function () {
		Route::get('reset', 'showLinkRequestForm')->name('request');
		Route::post('email', 'sendResetLinkEmail')->name('email');
		Route::get('reset/{token}', 'showResetForm')->name('reset');
		Route::post('reset', 'reset')->name('update');
	});
});

Route::group(['prefix' => 'ckeditor', 'as' => 'ckeditor.'], function(){
	Route::controller(WebsiteController::class)->group(function () {
		Route::post('/upload', 'upload')->name('upload');
	});
});


Route::group(['prefix' => 'blogs', 'as' => 'blogs.'], function(){
	Route::controller(BlogController::class)->group(function () {
		Route::get('', 'index')->name('index');
		Route::get('list', 'list')->name('list');
		Route::get('add', 'add')->name('add');
		Route::get('edit/{id?}', 'edit')->name('edit');
		Route::post('save/{id?}', 'save')->name('save');
		Route::get('find/{id?}', 'find')->name('find');
		Route::delete('delete/{id?}', 'delete')->name('delete');
	});
});

Route::group(['prefix' => 'test', 'as' => 'test.'], function(){
	Route::controller(SEOKeywordController::class)->group(function () {
		Route::get('link', 'extractProductDetails')->name('test');
	});
});


Route::group(['middleware' => 'role:admin'], function () {
		
});

Route::group(['middleware' => 'role:user'], function () {
    // User routes/controllers here
});