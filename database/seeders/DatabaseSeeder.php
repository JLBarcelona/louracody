<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::create([
            'name' => 'JL Barcelona',
            'full_permanent_address' => 'Sagana Santiago City Isabela',
            'gcash_name' => 'John Luis Barcelona',
            'gcash_number' => '09754952519',
            'id_number' => '12345678',
            'valid_id_type' => 'Drivers License',
            'valid_id_photo_front' => '',
            'valid_id_photo_back' => '',
            'is_verified' => 1,
            'is_shop' => 0,
            'role_id' => 1,
            'user_type' => 1,
            'email' => 'johnluisb14@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Greatwhite18!')
        ]);



        $role = Role::create(['name' => 'admin']);
    }
}
