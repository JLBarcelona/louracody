<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('full_permanent_address');
            $table->string('gcash_name')->nullable();
            $table->string('gcash_number')->nullable();
            $table->string('id_number')->nullable();
            $table->string('valid_id_type')->nullable();
            $table->string('valid_id_photo_front')->nullable();
            $table->string('valid_id_photo_back')->nullable();
            $table->integer('is_verified');
            $table->integer('is_shop');
            $table->integer('user_type');
            $table->integer('shop_id')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('role_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
